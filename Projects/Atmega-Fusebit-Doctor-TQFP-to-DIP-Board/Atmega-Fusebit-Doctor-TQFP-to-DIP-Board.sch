EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Components
LIBS:Connectors
LIBS:Devices
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "5 apr 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L HEADER-13X2 P1
U 1 1 55219A57
P 6950 2200
F 0 "P1" V 6900 2200 40  0000 C CNN
F 1 "HEADER-13X2" V 7000 2200 40  0000 C CNN
F 2 "~" H 6950 2250 60  0000 C CNN
F 3 "~" H 6950 2250 60  0000 C CNN
	1    6950 2200
	1    0    0    -1  
$EndComp
$Comp
L HEADER-13X2 P3
U 1 1 55219A66
P 8400 2200
F 0 "P3" V 8350 2200 40  0000 C CNN
F 1 "HEADER-13X2" V 8450 2200 40  0000 C CNN
F 2 "~" H 8400 2250 60  0000 C CNN
F 3 "~" H 8400 2250 60  0000 C CNN
	1    8400 2200
	1    0    0    -1  
$EndComp
$Comp
L HEADER-13X2 P2
U 1 1 55219A75
P 6950 3700
F 0 "P2" V 6900 3700 40  0000 C CNN
F 1 "HEADER-13X2" V 7000 3700 40  0000 C CNN
F 2 "~" H 6950 3750 60  0000 C CNN
F 3 "~" H 6950 3750 60  0000 C CNN
	1    6950 3700
	1    0    0    -1  
$EndComp
$Comp
L HEADER-13X2 P4
U 1 1 55219A84
P 8400 3700
F 0 "P4" V 8350 3700 40  0000 C CNN
F 1 "HEADER-13X2" V 8450 3700 40  0000 C CNN
F 2 "~" H 8400 3750 60  0000 C CNN
F 3 "~" H 8400 3750 60  0000 C CNN
	1    8400 3700
	1    0    0    -1  
$EndComp
Text Label 3050 1450 0    39   ~ 0
AVCC
Text Label 4250 1050 1    39   ~ 0
VCC3
Text Label 4350 1050 1    39   ~ 0
VCC2
Text Label 4450 1050 1    39   ~ 0
VCC1
Text Label 3050 1550 0    39   ~ 0
AREF
Wire Wire Line
	3000 1550 3200 1550
Wire Wire Line
	3000 1450 3200 1450
Wire Wire Line
	4250 850  4250 1050
Wire Wire Line
	4350 850  4350 1050
Wire Wire Line
	4450 1050 4450 900 
Wire Wire Line
	3050 6450 3200 6450
Wire Wire Line
	3200 6550 3050 6550
Text Label 3050 6550 0    39   ~ 0
XTAL1
Text Label 3050 6450 0    39   ~ 0
XTAL2
Text Label 7200 1600 0    39   ~ 0
AVCC
Wire Wire Line
	7200 1600 7350 1600
Text Label 7200 1700 0    39   ~ 0
AREF
Wire Wire Line
	7200 1700 7350 1700
Text Label 7200 2600 0    39   ~ 0
VCC4
Wire Wire Line
	7200 2600 7350 2600
Wire Wire Line
	8000 2500 8150 2500
Wire Wire Line
	8800 2400 8650 2400
Text Label 8650 2400 0    39   ~ 0
XTAL1
Text Label 8000 2500 0    39   ~ 0
XTAL2
Text Label 4500 7100 1    39   ~ 0
GND1
Text Label 4400 7100 1    39   ~ 0
GND2
Text Label 4300 7100 1    39   ~ 0
GND3
Text Label 4200 7100 1    39   ~ 0
GND4
Wire Wire Line
	4200 6950 4200 7150
Wire Wire Line
	4300 6950 4300 7100
Wire Wire Line
	4400 6950 4400 7150
Wire Wire Line
	4500 6950 4500 7100
Text Label 6550 2600 0    39   ~ 0
GND4
Text Label 6550 1700 0    39   ~ 0
GND5
Wire Wire Line
	6550 1700 6700 1700
Wire Wire Line
	6700 2600 6550 2600
Text Label 5400 1450 0    39   ~ 0
PA0
Text Label 5400 1550 0    39   ~ 0
PA1
Text Label 5400 1650 0    39   ~ 0
PA2
Text Label 5400 1750 0    39   ~ 0
PA3
Text Label 5400 1850 0    39   ~ 0
PA4
Text Label 5400 1950 0    39   ~ 0
PA5
Text Label 5400 2050 0    39   ~ 0
PA6
Text Label 5400 2150 0    39   ~ 0
PA7
Text Label 5400 2350 0    39   ~ 0
PC0
Text Label 5400 2450 0    39   ~ 0
PC1
Text Label 5400 2550 0    39   ~ 0
PC2
Text Label 5400 2650 0    39   ~ 0
PC3
Text Label 5400 2750 0    39   ~ 0
PC4
Text Label 5400 2850 0    39   ~ 0
PC5
Text Label 5400 2950 0    39   ~ 0
PC6
Text Label 5400 3050 0    39   ~ 0
PC7
Text Label 5400 3250 0    39   ~ 0
PE0
Text Label 5400 3350 0    39   ~ 0
PE1
Text Label 5400 3450 0    39   ~ 0
PE2
Text Label 5400 3550 0    39   ~ 0
PE3
Text Label 5400 3650 0    39   ~ 0
PE4
Text Label 5400 3750 0    39   ~ 0
PE5
Text Label 5400 3850 0    39   ~ 0
PE6
Text Label 5400 3950 0    39   ~ 0
PE7
Text Label 5400 4150 0    39   ~ 0
PG0
Text Label 5400 4250 0    39   ~ 0
PG1
Text Label 5400 4350 0    39   ~ 0
PG2
Text Label 5400 4450 0    39   ~ 0
PG3
Text Label 5400 4550 0    39   ~ 0
PG4
Text Label 5400 4650 0    39   ~ 0
PG5
Text Label 5400 4850 0    39   ~ 0
PJ0
Text Label 5400 4950 0    39   ~ 0
PJ1
Text Label 5400 5050 0    39   ~ 0
PJ2
Text Label 5400 5150 0    39   ~ 0
PJ3
Text Label 5400 5250 0    39   ~ 0
PJ4
Text Label 5400 5350 0    39   ~ 0
PJ5
Text Label 5400 5450 0    39   ~ 0
PJ6
Text Label 5400 5550 0    39   ~ 0
PJ7
Text Label 5400 5750 0    39   ~ 0
PL0
Text Label 5400 5850 0    39   ~ 0
PL1
Text Label 5400 5950 0    39   ~ 0
PL2
Text Label 5400 6050 0    39   ~ 0
PL3
Text Label 5400 6150 0    39   ~ 0
PL4
Text Label 5400 6250 0    39   ~ 0
PL5
Text Label 5400 6350 0    39   ~ 0
PL6
Text Label 5400 6450 0    39   ~ 0
PL7
Wire Wire Line
	5500 6450 5400 6450
Wire Wire Line
	5500 6350 5400 6350
Wire Wire Line
	5500 6250 5400 6250
Wire Wire Line
	5400 6150 5500 6150
Wire Wire Line
	5500 6050 5400 6050
Wire Wire Line
	5400 5950 5500 5950
Wire Wire Line
	5500 5850 5400 5850
Wire Wire Line
	5400 5750 5500 5750
Wire Wire Line
	5500 5550 5400 5550
Wire Wire Line
	5400 5450 5500 5450
Wire Wire Line
	5500 5350 5400 5350
Wire Wire Line
	5400 5250 5500 5250
Wire Wire Line
	5500 5150 5400 5150
Wire Wire Line
	5400 5050 5500 5050
Wire Wire Line
	5500 4950 5400 4950
Wire Wire Line
	5400 4850 5500 4850
Wire Wire Line
	5500 4650 5400 4650
Wire Wire Line
	5400 4550 5500 4550
Wire Wire Line
	5500 4450 5400 4450
Wire Wire Line
	5400 4350 5500 4350
Wire Wire Line
	5500 4250 5400 4250
Wire Wire Line
	5400 4150 5500 4150
Wire Wire Line
	5500 3950 5400 3950
Wire Wire Line
	5400 3850 5500 3850
Wire Wire Line
	5500 3750 5400 3750
Wire Wire Line
	5400 3650 5500 3650
Wire Wire Line
	5500 3550 5400 3550
Wire Wire Line
	5400 3450 5500 3450
Wire Wire Line
	5500 3350 5400 3350
Wire Wire Line
	5400 3250 5500 3250
Wire Wire Line
	5500 3050 5400 3050
Wire Wire Line
	5400 2950 5500 2950
Wire Wire Line
	5500 2850 5400 2850
Wire Wire Line
	5400 2750 5500 2750
Wire Wire Line
	5500 2650 5400 2650
Wire Wire Line
	5400 2550 5500 2550
Wire Wire Line
	5500 2450 5400 2450
Wire Wire Line
	5400 2350 5500 2350
Wire Wire Line
	5500 2150 5400 2150
Wire Wire Line
	5500 2050 5400 2050
Wire Wire Line
	5400 1950 5500 1950
Wire Wire Line
	5500 1850 5400 1850
Wire Wire Line
	5400 1750 5500 1750
Wire Wire Line
	5500 1650 5400 1650
Wire Wire Line
	5400 1550 5500 1550
Wire Wire Line
	5500 1450 5400 1450
Text Label 7200 2700 0    39   ~ 0
PA0
Text Label 6600 2800 0    39   ~ 0
PA1
Text Label 7200 2800 0    39   ~ 0
PA2
Text Label 7200 3100 0    39   ~ 0
PA3
Text Label 6600 3200 0    39   ~ 0
PA4
Text Label 7200 3200 0    39   ~ 0
PA5
Text Label 6600 3300 0    39   ~ 0
PA6
Text Label 7200 3300 0    39   ~ 0
PA7
Text Label 7200 4200 0    39   ~ 0
PC0
Text Label 6600 4200 0    39   ~ 0
PC1
Text Label 7200 4100 0    39   ~ 0
PC2
Text Label 6600 4100 0    39   ~ 0
PC3
Text Label 7200 4000 0    39   ~ 0
PC4
Text Label 6600 4000 0    39   ~ 0
PC5
Text Label 7200 3900 0    39   ~ 0
PC6
Text Label 6600 3900 0    39   ~ 0
PC7
Text Label 8050 4300 0    39   ~ 0
PE0
Text Label 8650 4200 0    39   ~ 0
PE1
Text Label 8050 4200 0    39   ~ 0
PE2
Text Label 8650 4100 0    39   ~ 0
PE3
Text Label 8050 4100 0    39   ~ 0
PE4
Text Label 8650 4000 0    39   ~ 0
PE5
Text Label 8050 4000 0    39   ~ 0
PE6
Text Label 8650 3900 0    39   ~ 0
PE7
Text Label 7200 4300 0    39   ~ 0
PG0
Text Label 6600 4300 0    39   ~ 0
PG1
Text Label 6600 3400 0    39   ~ 0
PG2
Text Label 8650 2700 0    39   ~ 0
PG3
Text Label 8050 2700 0    39   ~ 0
PG4
Text Label 8650 4300 0    39   ~ 0
PG5
Text Label 7200 3700 0    39   ~ 0
PJ0
Text Label 6600 3700 0    39   ~ 0
PJ1
Text Label 7200 3600 0    39   ~ 0
PJ2
Text Label 6600 3600 0    39   ~ 0
PJ3
Text Label 7200 3500 0    39   ~ 0
PJ4
Text Label 6600 3500 0    39   ~ 0
PJ5
Text Label 7200 3400 0    39   ~ 0
PJ6
Text Label 6600 2700 0    39   ~ 0
PJ7
Text Label 8050 2400 0    39   ~ 0
PL0
Text Label 8650 2300 0    39   ~ 0
PL1
Text Label 8050 2300 0    39   ~ 0
PL2
Text Label 8650 2200 0    39   ~ 0
PL3
Text Label 8050 2200 0    39   ~ 0
PL4
Text Label 8650 2100 0    39   ~ 0
PL5
Text Label 8050 2100 0    39   ~ 0
PL6
Text Label 8650 2000 0    39   ~ 0
PL7
Wire Wire Line
	8750 2000 8650 2000
Wire Wire Line
	8150 2100 8050 2100
Wire Wire Line
	8750 2100 8650 2100
Wire Wire Line
	8050 2200 8150 2200
Wire Wire Line
	8750 2200 8650 2200
Wire Wire Line
	8050 2300 8150 2300
Wire Wire Line
	8750 2300 8650 2300
Wire Wire Line
	8050 2400 8150 2400
Wire Wire Line
	6700 2700 6600 2700
Wire Wire Line
	7200 3400 7300 3400
Wire Wire Line
	6700 3500 6600 3500
Wire Wire Line
	7200 3500 7300 3500
Wire Wire Line
	6700 3600 6600 3600
Wire Wire Line
	7200 3600 7300 3600
Wire Wire Line
	6700 3700 6600 3700
Wire Wire Line
	7200 3700 7300 3700
Wire Wire Line
	8750 4300 8650 4300
Wire Wire Line
	8050 2700 8150 2700
Wire Wire Line
	8750 2700 8650 2700
Wire Wire Line
	6600 3400 6700 3400
Wire Wire Line
	6700 4300 6600 4300
Wire Wire Line
	7200 4300 7300 4300
Wire Wire Line
	8750 3900 8650 3900
Wire Wire Line
	8050 4000 8150 4000
Wire Wire Line
	8750 4000 8650 4000
Wire Wire Line
	8050 4100 8150 4100
Wire Wire Line
	8750 4100 8650 4100
Wire Wire Line
	8050 4200 8150 4200
Wire Wire Line
	8750 4200 8650 4200
Wire Wire Line
	8050 4300 8150 4300
Wire Wire Line
	6700 3900 6600 3900
Wire Wire Line
	7200 3900 7300 3900
Wire Wire Line
	6700 4000 6600 4000
Wire Wire Line
	7200 4000 7300 4000
Wire Wire Line
	6700 4100 6600 4100
Wire Wire Line
	7200 4100 7300 4100
Wire Wire Line
	6700 4200 6600 4200
Wire Wire Line
	7200 4200 7300 4200
Wire Wire Line
	7300 3300 7200 3300
Wire Wire Line
	6700 3300 6600 3300
Wire Wire Line
	7200 3200 7300 3200
Wire Wire Line
	6700 3200 6600 3200
Wire Wire Line
	7200 3100 7300 3100
Wire Wire Line
	7300 2800 7200 2800
Wire Wire Line
	6600 2800 6700 2800
Wire Wire Line
	7300 2700 7200 2700
Text Label 3100 1750 0    39   ~ 0
PB0
Text Label 3100 1850 0    39   ~ 0
PB1
Text Label 3100 1950 0    39   ~ 0
PB2
Text Label 3100 2050 0    39   ~ 0
PB3
Text Label 3100 2150 0    39   ~ 0
PB4
Text Label 3100 2250 0    39   ~ 0
PB5
Text Label 3100 2350 0    39   ~ 0
PB6
Text Label 3100 2450 0    39   ~ 0
PB7
Text Label 3100 2650 0    39   ~ 0
PD0
Text Label 3100 2750 0    39   ~ 0
PD1
Text Label 3100 2850 0    39   ~ 0
PD2
Text Label 3100 2950 0    39   ~ 0
PD3
Text Label 3100 3050 0    39   ~ 0
PD4
Text Label 3100 3150 0    39   ~ 0
PD5
Text Label 3100 3250 0    39   ~ 0
PD6
Text Label 3100 3350 0    39   ~ 0
PD7
Text Label 3100 3550 0    39   ~ 0
PF0
Text Label 3100 3650 0    39   ~ 0
PF1
Text Label 3100 3750 0    39   ~ 0
PF2
Text Label 3100 3850 0    39   ~ 0
PF3
Text Label 3100 3950 0    39   ~ 0
PF4
Text Label 3100 4050 0    39   ~ 0
PF5
Text Label 3100 4150 0    39   ~ 0
PF6
Text Label 3100 4250 0    39   ~ 0
PF7
Text Label 3100 4450 0    39   ~ 0
PH0
Text Label 3100 4550 0    39   ~ 0
PH1
Text Label 3100 4650 0    39   ~ 0
PH2
Text Label 3100 4750 0    39   ~ 0
PH3
Text Label 3100 4850 0    39   ~ 0
PH4
Text Label 3100 4950 0    39   ~ 0
PH5
Text Label 3100 5050 0    39   ~ 0
PH6
Text Label 3100 5150 0    39   ~ 0
PH7
Text Label 3100 5350 0    39   ~ 0
PK0
Text Label 3100 5450 0    39   ~ 0
PK1
Text Label 3100 5550 0    39   ~ 0
PK2
Text Label 3100 5650 0    39   ~ 0
PK3
Text Label 3100 5750 0    39   ~ 0
PK4
Text Label 3100 5850 0    39   ~ 0
PK5
Text Label 3100 5950 0    39   ~ 0
PK6
Text Label 3100 6050 0    39   ~ 0
PK7
Text Label 3100 6250 0    39   ~ 0
RST
Wire Wire Line
	3100 6250 3200 6250
Wire Wire Line
	3200 6050 3100 6050
Wire Wire Line
	3100 5950 3200 5950
Wire Wire Line
	3200 5850 3100 5850
Wire Wire Line
	3100 5750 3200 5750
Wire Wire Line
	3200 5650 3100 5650
Wire Wire Line
	3100 5550 3200 5550
Wire Wire Line
	3200 5450 3100 5450
Wire Wire Line
	3100 5350 3200 5350
Wire Wire Line
	3200 5150 3100 5150
Wire Wire Line
	3100 5050 3200 5050
Wire Wire Line
	3200 4950 3100 4950
Wire Wire Line
	3100 4850 3200 4850
Wire Wire Line
	3200 4750 3100 4750
Wire Wire Line
	3200 4650 3100 4650
Wire Wire Line
	3100 4550 3200 4550
Wire Wire Line
	3200 4450 3100 4450
Wire Wire Line
	3100 4250 3200 4250
Wire Wire Line
	3200 4150 3100 4150
Wire Wire Line
	3100 4050 3200 4050
Wire Wire Line
	3200 3950 3100 3950
Wire Wire Line
	3100 3850 3200 3850
Wire Wire Line
	3200 3750 3100 3750
Wire Wire Line
	3100 3650 3200 3650
Wire Wire Line
	3200 3550 3100 3550
Wire Wire Line
	3100 3350 3200 3350
Wire Wire Line
	3200 3250 3100 3250
Wire Wire Line
	3100 3150 3200 3150
Wire Wire Line
	3200 3050 3100 3050
Wire Wire Line
	3100 2950 3200 2950
Wire Wire Line
	3200 2850 3100 2850
Wire Wire Line
	3100 2750 3200 2750
Wire Wire Line
	3200 2650 3100 2650
Wire Wire Line
	3100 2450 3200 2450
Wire Wire Line
	3200 2350 3100 2350
Wire Wire Line
	3100 2250 3200 2250
Wire Wire Line
	3200 2150 3100 2150
Wire Wire Line
	3100 2050 3200 2050
Wire Wire Line
	3200 1950 3100 1950
Wire Wire Line
	3100 1850 3200 1850
Wire Wire Line
	3200 1750 3100 1750
Text Label 8650 3400 0    39   ~ 0
PB0
Text Label 8050 3400 0    39   ~ 0
PB1
Text Label 8650 3300 0    39   ~ 0
PB2
Text Label 8050 3300 0    39   ~ 0
PB3
Text Label 8650 3200 0    39   ~ 0
PB4
Text Label 8050 3200 0    39   ~ 0
PB5
Text Label 8650 3100 0    39   ~ 0
PB6
Text Label 8650 2800 0    39   ~ 0
PB7
Text Label 8050 2000 0    39   ~ 0
PD0
Text Label 8650 1900 0    39   ~ 0
PD1
Text Label 8050 1900 0    39   ~ 0
PD2
Text Label 8650 1800 0    39   ~ 0
PD3
Text Label 8050 1800 0    39   ~ 0
PD4
Text Label 8650 1700 0    39   ~ 0
PD5
Text Label 8050 1700 0    39   ~ 0
PD6
Text Label 8650 1600 0    39   ~ 0
PD7
Text Label 6600 1800 0    39   ~ 0
PF0
Text Label 7200 1800 0    39   ~ 0
PF1
Text Label 6600 1900 0    39   ~ 0
PF2
Text Label 7200 1900 0    39   ~ 0
PF3
Text Label 6600 2000 0    39   ~ 0
PF4
Text Label 7200 2000 0    39   ~ 0
PF5
Text Label 6600 2100 0    39   ~ 0
PF6
Text Label 7200 2100 0    39   ~ 0
PF7
Text Label 8050 3800 0    39   ~ 0
PH0
Text Label 8650 3700 0    39   ~ 0
PH1
Text Label 8050 3700 0    39   ~ 0
PH2
Text Label 8650 3600 0    39   ~ 0
PH3
Text Label 8050 3600 0    39   ~ 0
PH4
Text Label 8650 3500 0    39   ~ 0
PH5
Text Label 8050 3500 0    39   ~ 0
PH6
Text Label 8050 2800 0    39   ~ 0
PH7
Text Label 6600 2200 0    39   ~ 0
PK0
Text Label 7200 2200 0    39   ~ 0
PK1
Text Label 6600 2300 0    39   ~ 0
PK2
Text Label 7200 2300 0    39   ~ 0
PK3
Text Label 6600 2400 0    39   ~ 0
PK4
Text Label 7200 2400 0    39   ~ 0
PK5
Text Label 6600 2500 0    39   ~ 0
PK6
Text Label 7200 2500 0    39   ~ 0
PK7
Text Label 8650 2600 0    39   ~ 0
RST
Wire Wire Line
	8650 2600 8750 2600
Wire Wire Line
	7300 2500 7200 2500
Wire Wire Line
	6600 2500 6700 2500
Wire Wire Line
	7300 2400 7200 2400
Wire Wire Line
	6600 2400 6700 2400
Wire Wire Line
	7300 2300 7200 2300
Wire Wire Line
	6600 2300 6700 2300
Wire Wire Line
	7300 2200 7200 2200
Wire Wire Line
	6600 2200 6700 2200
Wire Wire Line
	8150 2800 8050 2800
Wire Wire Line
	8050 3500 8150 3500
Wire Wire Line
	8750 3500 8650 3500
Wire Wire Line
	8050 3600 8150 3600
Wire Wire Line
	8750 3600 8650 3600
Wire Wire Line
	8150 3700 8050 3700
Wire Wire Line
	8650 3700 8750 3700
Wire Wire Line
	8150 3800 8050 3800
Wire Wire Line
	7200 2100 7300 2100
Wire Wire Line
	6700 2100 6600 2100
Wire Wire Line
	7200 2000 7300 2000
Wire Wire Line
	6700 2000 6600 2000
Wire Wire Line
	7200 1900 7300 1900
Wire Wire Line
	6700 1900 6600 1900
Wire Wire Line
	7200 1800 7300 1800
Wire Wire Line
	6700 1800 6600 1800
Wire Wire Line
	8650 1600 8750 1600
Wire Wire Line
	8150 1700 8050 1700
Wire Wire Line
	8650 1700 8750 1700
Wire Wire Line
	8150 1800 8050 1800
Wire Wire Line
	8650 1800 8750 1800
Wire Wire Line
	8150 1900 8050 1900
Wire Wire Line
	8650 1900 8750 1900
Wire Wire Line
	8150 2000 8050 2000
Wire Wire Line
	8650 2800 8750 2800
Wire Wire Line
	8750 3100 8650 3100
Wire Wire Line
	8050 3200 8150 3200
Wire Wire Line
	8750 3200 8650 3200
Wire Wire Line
	8050 3300 8150 3300
Wire Wire Line
	8750 3300 8650 3300
Wire Wire Line
	8050 3400 8150 3400
Wire Wire Line
	8750 3400 8650 3400
Text Label 8800 3800 2    39   ~ 0
GND1
Text Label 8800 2500 2    39   ~ 0
GND2
Wire Wire Line
	8800 2500 8650 2500
Wire Wire Line
	8650 3800 8800 3800
Text Label 4100 7100 1    39   ~ 0
GND5
Wire Wire Line
	4100 7100 4100 6950
Text Label 6700 3800 2    39   ~ 0
GND3
Wire Wire Line
	6550 3800 6700 3800
Wire Wire Line
	4150 1050 4150 900 
Text Label 4150 1050 1    39   ~ 0
VCC4
Text Label 8150 2600 2    39   ~ 0
VCC2
Text Label 8150 3900 2    39   ~ 0
VCC1
Text Label 7200 3800 0    39   ~ 0
VCC3
Wire Wire Line
	7200 3800 7350 3800
Wire Wire Line
	8000 2600 8150 2600
Wire Wire Line
	8150 3900 8000 3900
NoConn ~ 8150 3100
NoConn ~ 6700 3100
NoConn ~ 6700 1600
NoConn ~ 8150 1600
$Comp
L PWR_FLAG #FLG6
U 1 1 55220533
P 4150 900
F 0 "#FLG6" H 4150 995 30  0001 C CNN
F 1 "PWR_FLAG" H 4150 1080 30  0000 C CNN
F 2 "" H 4150 900 60  0000 C CNN
F 3 "" H 4150 900 60  0000 C CNN
	1    4150 900 
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG9
U 1 1 55220542
P 4450 900
F 0 "#FLG9" H 4450 995 30  0001 C CNN
F 1 "PWR_FLAG" H 4450 1080 30  0000 C CNN
F 2 "" H 4450 900 60  0000 C CNN
F 3 "" H 4450 900 60  0000 C CNN
	1    4450 900 
	0    1    1    0   
$EndComp
$Comp
L PWR_FLAG #FLG5
U 1 1 55220556
P 4150 850
F 0 "#FLG5" H 4150 945 30  0001 C CNN
F 1 "PWR_FLAG" H 4150 1030 30  0000 C CNN
F 2 "" H 4150 850 60  0000 C CNN
F 3 "" H 4150 850 60  0000 C CNN
	1    4150 850 
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG8
U 1 1 55220565
P 4450 850
F 0 "#FLG8" H 4450 945 30  0001 C CNN
F 1 "PWR_FLAG" H 4450 1030 30  0000 C CNN
F 2 "" H 4450 850 60  0000 C CNN
F 3 "" H 4450 850 60  0000 C CNN
	1    4450 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 850  4350 850 
Wire Wire Line
	4150 850  4250 850 
$Comp
L PWR_FLAG #FLG3
U 1 1 55220707
P 4100 7100
F 0 "#FLG3" H 4100 7195 30  0001 C CNN
F 1 "PWR_FLAG" H 4100 7280 30  0000 C CNN
F 2 "" H 4100 7100 60  0000 C CNN
F 3 "" H 4100 7100 60  0000 C CNN
	1    4100 7100
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG10
U 1 1 55220716
P 4500 7100
F 0 "#FLG10" H 4500 7195 30  0001 C CNN
F 1 "PWR_FLAG" H 4500 7280 30  0000 C CNN
F 2 "" H 4500 7100 60  0000 C CNN
F 3 "" H 4500 7100 60  0000 C CNN
	1    4500 7100
	0    1    1    0   
$EndComp
$Comp
L PWR_FLAG #FLG4
U 1 1 55220725
P 4100 7150
F 0 "#FLG4" H 4100 7245 30  0001 C CNN
F 1 "PWR_FLAG" H 4100 7330 30  0000 C CNN
F 2 "" H 4100 7150 60  0000 C CNN
F 3 "" H 4100 7150 60  0000 C CNN
	1    4100 7150
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG11
U 1 1 55220734
P 4500 7150
F 0 "#FLG11" H 4500 7245 30  0001 C CNN
F 1 "PWR_FLAG" H 4500 7330 30  0000 C CNN
F 2 "" H 4500 7150 60  0000 C CNN
F 3 "" H 4500 7150 60  0000 C CNN
	1    4500 7150
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG7
U 1 1 55220743
P 4300 7100
F 0 "#FLG7" H 4300 7195 30  0001 C CNN
F 1 "PWR_FLAG" H 4300 7280 30  0000 C CNN
F 2 "" H 4300 7100 60  0000 C CNN
F 3 "" H 4300 7100 60  0000 C CNN
	1    4300 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4400 7150 4500 7150
Wire Wire Line
	4200 7150 4100 7150
$Comp
L PWR_FLAG #FLG1
U 1 1 55220B74
P 3000 1400
F 0 "#FLG1" H 3000 1495 30  0001 C CNN
F 1 "PWR_FLAG" H 3000 1580 30  0000 C CNN
F 2 "" H 3000 1400 60  0000 C CNN
F 3 "" H 3000 1400 60  0000 C CNN
	1    3000 1400
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG2
U 1 1 55220B83
P 3000 1550
F 0 "#FLG2" H 3000 1645 30  0001 C CNN
F 1 "PWR_FLAG" H 3000 1730 30  0000 C CNN
F 2 "" H 3000 1550 60  0000 C CNN
F 3 "" H 3000 1550 60  0000 C CNN
	1    3000 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 1400 3000 1450
$Comp
L HEADER-20X1 P5
U 1 1 5521AD64
P 1450 2800
F 0 "P5" V 1400 2800 40  0000 C CNN
F 1 "HEADER-20X1" V 1500 2800 40  0000 C CNN
F 2 "~" H 1450 3300 60  0000 C CNN
F 3 "~" H 1450 3300 60  0000 C CNN
	1    1450 2800
	1    0    0    -1  
$EndComp
$Comp
L HEADER-20X1 P6
U 1 1 5521AD73
P 2300 2800
F 0 "P6" V 2250 2800 40  0000 C CNN
F 1 "HEADER-20X1" V 2350 2800 40  0000 C CNN
F 2 "~" H 2300 3300 60  0000 C CNN
F 3 "~" H 2300 3300 60  0000 C CNN
	1    2300 2800
	1    0    0    -1  
$EndComp
$Comp
L ATMEGA2560-TQFP100 IC1
U 1 1 5521EDEE
P 4300 4250
F 0 "IC1" H 3400 7350 40  0000 C CNN
F 1 "ATMEGA2560-TQFP100" H 3350 1600 40  0000 L BNN
F 2 "TQFP-100" H 4300 4300 30  0000 C CIN
F 3 "~" H 4500 4450 60  0000 C CNN
	1    4300 4250
	1    0    0    -1  
$EndComp
Text Label 1100 1850 0    39   ~ 0
PB0
Text Label 1100 1950 0    39   ~ 0
PB1
Text Label 1100 2050 0    39   ~ 0
PB2
Text Label 1100 2150 0    39   ~ 0
PB3
Text Label 1100 2250 0    39   ~ 0
PB4
Text Label 1100 2350 0    39   ~ 0
PB5
Text Label 1100 2450 0    39   ~ 0
PB6
Text Label 1100 2550 0    39   ~ 0
PB7
Wire Wire Line
	1100 2550 1200 2550
Wire Wire Line
	1200 2450 1100 2450
Wire Wire Line
	1100 2350 1200 2350
Wire Wire Line
	1200 2250 1100 2250
Wire Wire Line
	1100 2150 1200 2150
Wire Wire Line
	1200 2050 1100 2050
Wire Wire Line
	1100 1950 1200 1950
Wire Wire Line
	1200 1850 1100 1850
Wire Wire Line
	1200 3050 1050 3050
Text Label 1050 3050 0    39   ~ 0
XTAL1
$EndSCHEMATC
