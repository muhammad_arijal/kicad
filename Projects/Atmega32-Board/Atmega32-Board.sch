EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Components
LIBS:Connectors
LIBS:Devices
LIBS:Atmega32-Board-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "5 apr 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA32A-PDIP40 IC1
U 1 1 55216701
P 4200 2500
F 0 "IC1" H 3640 3790 40  0000 C CNN
F 1 "ATMEGA32A-PDIP40" H 3900 1200 40  0000 C CNN
F 2 "PDIP-40" H 4200 2500 30  0000 C CIN
F 3 "~" H 4200 1950 60  0000 C CNN
	1    4200 2500
	1    0    0    -1  
$EndComp
$Comp
L HEADER-20X1 P1
U 1 1 55216739
P 1600 2200
F 0 "P1" V 1550 2200 40  0000 C CNN
F 1 "HEADER-20X1" V 1650 2200 40  0000 C CNN
F 2 "~" H 1600 2700 60  0000 C CNN
F 3 "~" H 1600 2700 60  0000 C CNN
	1    1600 2200
	1    0    0    -1  
$EndComp
$Comp
L HEADER-20X1 P2
U 1 1 55216748
P 2550 2200
F 0 "P2" V 2500 2200 40  0000 C CNN
F 1 "HEADER-20X1" V 2600 2200 40  0000 C CNN
F 2 "~" H 2550 2700 60  0000 C CNN
F 3 "~" H 2550 2700 60  0000 C CNN
	1    2550 2200
	1    0    0    -1  
$EndComp
Text Label 3350 1350 0    39   ~ 0
PB0
Text Label 3350 1450 0    39   ~ 0
PB1
Text Label 3350 1550 0    39   ~ 0
PB2
Text Label 3350 1650 0    39   ~ 0
PB3
Text Label 3350 1750 0    39   ~ 0
PB4
Text Label 3350 1850 0    39   ~ 0
PB5
Text Label 3350 1950 0    39   ~ 0
PB6
Text Label 3350 2050 0    39   ~ 0
PB7
Text Label 3350 3150 0    39   ~ 0
RST
Text Label 4950 3150 0    39   ~ 0
VCC
Text Label 4950 3250 0    39   ~ 0
AVCC
Text Label 3300 3350 0    39   ~ 0
XTAL2
Text Label 3300 3450 0    39   ~ 0
XTAL1
Text Label 1250 1250 0    39   ~ 0
PB0
Text Label 1250 1350 0    39   ~ 0
PB1
Text Label 1250 1450 0    39   ~ 0
PB2
Text Label 1250 1550 0    39   ~ 0
PB3
Text Label 1250 1650 0    39   ~ 0
PB4
Text Label 1250 1750 0    39   ~ 0
PB5
Text Label 1250 1850 0    39   ~ 0
PB6
Text Label 1250 1950 0    39   ~ 0
PB7
Text Label 1250 2050 0    39   ~ 0
RST
Text Label 1250 2150 0    39   ~ 0
VCC
Text Label 4950 3550 0    39   ~ 0
GND1
Text Label 1200 2250 0    39   ~ 0
GND1
Text Label 1200 2350 0    39   ~ 0
XTAL2
Text Label 1200 2450 0    39   ~ 0
XTAL1
Text Label 4950 1350 0    39   ~ 0
PA0
Text Label 4950 1450 0    39   ~ 0
PA1
Text Label 4950 1550 0    39   ~ 0
PA2
Text Label 4950 1650 0    39   ~ 0
PA3
Text Label 4950 1750 0    39   ~ 0
PA4
Text Label 4950 1850 0    39   ~ 0
PA5
Text Label 4950 1950 0    39   ~ 0
PA6
Text Label 4950 2050 0    39   ~ 0
PA7
Wire Wire Line
	3300 3350 3450 3350
Wire Wire Line
	3450 3450 3300 3450
Wire Wire Line
	3350 3150 3450 3150
Wire Wire Line
	3350 1350 3450 1350
Wire Wire Line
	3350 1450 3450 1450
Wire Wire Line
	3450 1550 3350 1550
Wire Wire Line
	3450 1650 3350 1650
Wire Wire Line
	3450 1750 3350 1750
Wire Wire Line
	3450 1850 3350 1850
Wire Wire Line
	3350 1950 3450 1950
Wire Wire Line
	3450 2050 3350 2050
Wire Wire Line
	1250 1250 1350 1250
Wire Wire Line
	1250 1350 1350 1350
Wire Wire Line
	1350 1450 1250 1450
Wire Wire Line
	1350 1550 1250 1550
Wire Wire Line
	1350 1650 1250 1650
Wire Wire Line
	1350 1750 1250 1750
Wire Wire Line
	1250 1850 1350 1850
Wire Wire Line
	1350 1950 1250 1950
Wire Wire Line
	1250 2050 1350 2050
Wire Wire Line
	4950 3150 5050 3150
Wire Wire Line
	4950 3250 5100 3250
Wire Wire Line
	1250 2150 1350 2150
Wire Wire Line
	1200 2250 1350 2250
Wire Wire Line
	1200 2350 1350 2350
Wire Wire Line
	1350 2450 1200 2450
Wire Wire Line
	4950 1350 5050 1350
Wire Wire Line
	5050 1450 4950 1450
Wire Wire Line
	4950 1550 5050 1550
Wire Wire Line
	5050 1650 4950 1650
Wire Wire Line
	5050 1750 4950 1750
Wire Wire Line
	5050 1850 4950 1850
Wire Wire Line
	5050 1950 4950 1950
Wire Wire Line
	5050 2050 4950 2050
Text Label 2200 1250 0    39   ~ 0
PA0
Text Label 2200 1350 0    39   ~ 0
PA1
Text Label 2200 1450 0    39   ~ 0
PA2
Text Label 2200 1550 0    39   ~ 0
PA3
Text Label 2200 1650 0    39   ~ 0
PA4
Text Label 2200 1750 0    39   ~ 0
PA5
Text Label 2200 1850 0    39   ~ 0
PA6
Text Label 2200 1950 0    39   ~ 0
PA7
Wire Wire Line
	2200 1250 2300 1250
Wire Wire Line
	2300 1350 2200 1350
Wire Wire Line
	2200 1450 2300 1450
Wire Wire Line
	2300 1550 2200 1550
Wire Wire Line
	2300 1650 2200 1650
Wire Wire Line
	2300 1750 2200 1750
Wire Wire Line
	2300 1850 2200 1850
Wire Wire Line
	2300 1950 2200 1950
Wire Wire Line
	4950 3650 5050 3650
Text Label 4950 3650 0    39   ~ 0
GND2
Text Label 4950 3350 0    39   ~ 0
AREF
Wire Wire Line
	4950 3350 5100 3350
Text Label 2150 2250 0    39   ~ 0
AVCC
Wire Wire Line
	2150 2250 2300 2250
Text Label 2150 2050 0    39   ~ 0
AREF
Wire Wire Line
	2150 2050 2300 2050
Wire Wire Line
	2150 2150 2300 2150
Text Label 2150 2150 0    39   ~ 0
GND2
Text Label 4950 2250 0    39   ~ 0
PC0
Text Label 4950 2350 0    39   ~ 0
PC1
Text Label 4950 2450 0    39   ~ 0
PC2
Text Label 4950 2550 0    39   ~ 0
PC3
Text Label 4950 2650 0    39   ~ 0
PC4
Text Label 4950 2750 0    39   ~ 0
PC5
Text Label 4950 2850 0    39   ~ 0
PC6
Text Label 4950 2950 0    39   ~ 0
PC7
Text Label 3350 2250 0    39   ~ 0
PD0
Text Label 3350 2350 0    39   ~ 0
PD1
Text Label 3350 2450 0    39   ~ 0
PD2
Text Label 3350 2550 0    39   ~ 0
PD3
Text Label 3350 2650 0    39   ~ 0
PD4
Text Label 3350 2750 0    39   ~ 0
PD5
Text Label 3350 2850 0    39   ~ 0
PD6
Text Label 3350 2950 0    39   ~ 0
PD7
Wire Wire Line
	3450 2950 3350 2950
Wire Wire Line
	3350 2850 3450 2850
Wire Wire Line
	3450 2750 3350 2750
Wire Wire Line
	3350 2650 3450 2650
Wire Wire Line
	3450 2550 3350 2550
Wire Wire Line
	3350 2450 3450 2450
Wire Wire Line
	3450 2350 3350 2350
Wire Wire Line
	3350 2250 3450 2250
Wire Wire Line
	4950 2250 5050 2250
Wire Wire Line
	5050 2350 4950 2350
Wire Wire Line
	4950 2450 5050 2450
Wire Wire Line
	5050 2550 4950 2550
Wire Wire Line
	4950 2650 5050 2650
Wire Wire Line
	5050 2750 4950 2750
Wire Wire Line
	4950 2850 5050 2850
Wire Wire Line
	5050 2950 4950 2950
Text Label 1250 2550 0    39   ~ 0
PD0
Text Label 1250 2650 0    39   ~ 0
PD1
Text Label 1250 2750 0    39   ~ 0
PD2
Text Label 1250 2850 0    39   ~ 0
PD3
Text Label 1250 2950 0    39   ~ 0
PD4
Text Label 1250 3050 0    39   ~ 0
PD5
Text Label 1250 3150 0    39   ~ 0
PD6
Text Label 2200 3150 0    39   ~ 0
PD7
Wire Wire Line
	2300 3150 2200 3150
Wire Wire Line
	1250 3150 1350 3150
Wire Wire Line
	1350 3050 1250 3050
Wire Wire Line
	1250 2950 1350 2950
Wire Wire Line
	1350 2850 1250 2850
Wire Wire Line
	1250 2750 1350 2750
Wire Wire Line
	1350 2650 1250 2650
Wire Wire Line
	1250 2550 1350 2550
Text Label 2200 3050 0    39   ~ 0
PC0
Text Label 2200 2950 0    39   ~ 0
PC1
Text Label 2200 2850 0    39   ~ 0
PC2
Text Label 2200 2750 0    39   ~ 0
PC3
Text Label 2200 2650 0    39   ~ 0
PC4
Text Label 2200 2550 0    39   ~ 0
PC5
Text Label 2200 2450 0    39   ~ 0
PC6
Text Label 2200 2350 0    39   ~ 0
PC7
Wire Wire Line
	2200 3050 2300 3050
Wire Wire Line
	2300 2950 2200 2950
Wire Wire Line
	2200 2850 2300 2850
Wire Wire Line
	2300 2750 2200 2750
Wire Wire Line
	2200 2650 2300 2650
Wire Wire Line
	2300 2550 2200 2550
Wire Wire Line
	2200 2450 2300 2450
Wire Wire Line
	2300 2350 2200 2350
$Comp
L PWR_FLAG #FLG01
U 1 1 552174D2
P 5050 3650
F 0 "#FLG01" H 5050 3745 30  0001 C CNN
F 1 "PWR_FLAG" H 5050 3830 30  0000 C CNN
F 2 "" H 5050 3650 60  0000 C CNN
F 3 "" H 5050 3650 60  0000 C CNN
	1    5050 3650
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 552174E1
P 5050 3150
F 0 "#FLG02" H 5050 3245 30  0001 C CNN
F 1 "PWR_FLAG" H 5050 3330 30  0000 C CNN
F 2 "" H 5050 3150 60  0000 C CNN
F 3 "" H 5050 3150 60  0000 C CNN
	1    5050 3150
	0    1    1    0   
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 552174F0
P 5100 3350
F 0 "#FLG03" H 5100 3445 30  0001 C CNN
F 1 "PWR_FLAG" H 5100 3530 30  0000 C CNN
F 2 "" H 5100 3350 60  0000 C CNN
F 3 "" H 5100 3350 60  0000 C CNN
	1    5100 3350
	0    1    1    0   
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 55216F31
P 5050 3550
F 0 "#FLG04" H 5050 3645 30  0001 C CNN
F 1 "PWR_FLAG" H 5050 3730 30  0000 C CNN
F 2 "" H 5050 3550 60  0000 C CNN
F 3 "" H 5050 3550 60  0000 C CNN
	1    5050 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 3550 4950 3550
$EndSCHEMATC
