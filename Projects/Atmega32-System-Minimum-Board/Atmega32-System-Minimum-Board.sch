EESchema Schematic File Version 2
LIBS:power
LIBS:Components
LIBS:Connectors
LIBS:Devices
LIBS:Atmega32-System-Minimum-Board-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "15 mar 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L HEADER-5X2 P1
U 1 1 53663DA5
P 2700 2300
F 0 "P1" V 2650 2300 40  0000 C CNN
F 1 "HEADER-5X2" V 2750 2300 40  0000 C CNN
F 2 "~" H 2700 2300 60  0000 C CNN
F 3 "~" H 2700 2300 60  0000 C CNN
	1    2700 2300
	1    0    0    -1  
$EndComp
$Comp
L HEADER-5X2 P2
U 1 1 53663DB4
P 2700 3200
F 0 "P2" V 2650 3200 40  0000 C CNN
F 1 "HEADER-5X2" V 2750 3200 40  0000 C CNN
F 2 "~" H 2700 3200 60  0000 C CNN
F 3 "~" H 2700 3200 60  0000 C CNN
	1    2700 3200
	1    0    0    -1  
$EndComp
$Comp
L HEADER-5X2 P3
U 1 1 53664155
P 5700 2300
F 0 "P3" V 5650 2300 40  0000 C CNN
F 1 "HEADER-5X2" V 5750 2300 40  0000 C CNN
F 2 "~" H 5700 2300 60  0000 C CNN
F 3 "~" H 5700 2300 60  0000 C CNN
	1    5700 2300
	1    0    0    -1  
$EndComp
Text Label 2300 2100 0    39   ~ 0
PB0
Text Label 2950 2100 0    39   ~ 0
PB1
Text Label 2300 2200 0    39   ~ 0
PB2
Text Label 2950 2200 0    39   ~ 0
PB3
Text Label 2300 2300 0    39   ~ 0
PB4
Text Label 2950 2300 0    39   ~ 0
PB5
Text Label 2300 2400 0    39   ~ 0
PB6
Text Label 2950 2400 0    39   ~ 0
PB7
Text Label 3300 1950 0    39   ~ 0
PB0
Text Label 3300 2050 0    39   ~ 0
PB1
Text Label 3300 2150 0    39   ~ 0
PB2
Text Label 3300 2250 0    39   ~ 0
PB3
Text Label 3300 2350 0    39   ~ 0
PB4
Text Label 3300 2450 0    39   ~ 0
PB5
Text Label 3300 2550 0    39   ~ 0
PB6
Text Label 3300 2650 0    39   ~ 0
PB7
Text Label 3300 2850 0    39   ~ 0
PD0
Text Label 3300 2950 0    39   ~ 0
PD1
Text Label 3300 3050 0    39   ~ 0
PD2
Text Label 3300 3150 0    39   ~ 0
PD3
Text Label 3300 3250 0    39   ~ 0
PD4
Text Label 3300 3350 0    39   ~ 0
PD5
Text Label 3300 3450 0    39   ~ 0
PD6
Text Label 3300 3550 0    39   ~ 0
PD7
Text Label 2300 3000 0    39   ~ 0
PD0
Text Label 2950 3000 0    39   ~ 0
PD1
Text Label 2300 3100 0    39   ~ 0
PD2
Text Label 2950 3100 0    39   ~ 0
PD3
Text Label 2300 3200 0    39   ~ 0
PD4
Text Label 2950 3200 0    39   ~ 0
PD5
Text Label 2300 3300 0    39   ~ 0
PD6
Text Label 2950 3300 0    39   ~ 0
PD7
Text Label 4950 1950 0    39   ~ 0
PA0
Text Label 4950 2050 0    39   ~ 0
PA1
Text Label 4950 2150 0    39   ~ 0
PA2
Text Label 4950 2250 0    39   ~ 0
PA3
Text Label 4950 2350 0    39   ~ 0
PA4
Text Label 4950 2450 0    39   ~ 0
PA5
Text Label 4950 2550 0    39   ~ 0
PA6
Text Label 4950 2650 0    39   ~ 0
PA7
Text Label 4950 2850 0    39   ~ 0
PC0
Text Label 4950 2950 0    39   ~ 0
PC1
Text Label 4950 3050 0    39   ~ 0
PC2
Text Label 4950 3150 0    39   ~ 0
PC3
Text Label 4950 3250 0    39   ~ 0
PC4
Text Label 4950 3350 0    39   ~ 0
PC5
Text Label 4950 3450 0    39   ~ 0
PC6
Text Label 4950 3550 0    39   ~ 0
PC7
Text Label 5300 2100 0    39   ~ 0
PA0
Text Label 5950 2100 0    39   ~ 0
PA1
Text Label 5300 2200 0    39   ~ 0
PA2
Text Label 5950 2200 0    39   ~ 0
PA3
Text Label 5300 2300 0    39   ~ 0
PA4
Text Label 5950 2300 0    39   ~ 0
PA5
Text Label 5300 2400 0    39   ~ 0
PA6
Text Label 5950 2400 0    39   ~ 0
PA7
Text Label 5300 3000 0    39   ~ 0
PC0
Text Label 5950 3000 0    39   ~ 0
PC1
Text Label 5300 3100 0    39   ~ 0
PC2
Text Label 5950 3100 0    39   ~ 0
PC3
Text Label 5300 3200 0    39   ~ 0
PC4
Text Label 5950 3200 0    39   ~ 0
PC5
Text Label 5300 3300 0    39   ~ 0
PC6
Text Label 5950 3300 0    39   ~ 0
PC7
$Comp
L VCC #PWR01
U 1 1 53665D96
P 2200 2400
F 0 "#PWR01" H 2200 2500 30  0001 C CNN
F 1 "VCC" H 2200 2500 30  0000 C CNN
F 2 "" H 2200 2400 60  0000 C CNN
F 3 "" H 2200 2400 60  0000 C CNN
	1    2200 2400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 53665DA5
P 2200 3300
F 0 "#PWR02" H 2200 3400 30  0001 C CNN
F 1 "VCC" H 2200 3400 30  0000 C CNN
F 2 "" H 2200 3300 60  0000 C CNN
F 3 "" H 2200 3300 60  0000 C CNN
	1    2200 3300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR03
U 1 1 53665DB4
P 5200 2400
F 0 "#PWR03" H 5200 2500 30  0001 C CNN
F 1 "VCC" H 5200 2500 30  0000 C CNN
F 2 "" H 5200 2400 60  0000 C CNN
F 3 "" H 5200 2400 60  0000 C CNN
	1    5200 2400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR04
U 1 1 53665DC3
P 5200 3300
F 0 "#PWR04" H 5200 3400 30  0001 C CNN
F 1 "VCC" H 5200 3400 30  0000 C CNN
F 2 "" H 5200 3300 60  0000 C CNN
F 3 "" H 5200 3300 60  0000 C CNN
	1    5200 3300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 53665DD2
P 6100 2600
F 0 "#PWR05" H 6100 2600 30  0001 C CNN
F 1 "GND" H 6100 2530 30  0001 C CNN
F 2 "" H 6100 2600 60  0000 C CNN
F 3 "" H 6100 2600 60  0000 C CNN
	1    6100 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 53665DE1
P 6100 3500
F 0 "#PWR06" H 6100 3500 30  0001 C CNN
F 1 "GND" H 6100 3430 30  0001 C CNN
F 2 "" H 6100 3500 60  0000 C CNN
F 3 "" H 6100 3500 60  0000 C CNN
	1    6100 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 53665DF0
P 3100 3500
F 0 "#PWR07" H 3100 3500 30  0001 C CNN
F 1 "GND" H 3100 3430 30  0001 C CNN
F 2 "" H 3100 3500 60  0000 C CNN
F 3 "" H 3100 3500 60  0000 C CNN
	1    3100 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 53665DFF
P 3100 2600
F 0 "#PWR08" H 3100 2600 30  0001 C CNN
F 1 "GND" H 3100 2530 30  0001 C CNN
F 2 "" H 3100 2600 60  0000 C CNN
F 3 "" H 3100 2600 60  0000 C CNN
	1    3100 2600
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG09
U 1 1 53666220
P 2100 2300
F 0 "#FLG09" H 2100 2395 30  0001 C CNN
F 1 "PWR_FLAG" H 2100 2480 30  0000 C CNN
F 2 "" H 2100 2300 60  0000 C CNN
F 3 "" H 2100 2300 60  0000 C CNN
	1    2100 2300
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG010
U 1 1 5366622F
P 6200 2600
F 0 "#FLG010" H 6200 2695 30  0001 C CNN
F 1 "PWR_FLAG" H 6200 2780 30  0000 C CNN
F 2 "" H 6200 2600 60  0000 C CNN
F 3 "" H 6200 2600 60  0000 C CNN
	1    6200 2600
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR011
U 1 1 536666D2
P 5150 3750
F 0 "#PWR011" H 5150 3850 30  0001 C CNN
F 1 "VCC" H 5150 3850 30  0000 C CNN
F 2 "" H 5150 3750 60  0000 C CNN
F 3 "" H 5150 3750 60  0000 C CNN
	1    5150 3750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 536666E1
P 5150 4350
F 0 "#PWR012" H 5150 4350 30  0001 C CNN
F 1 "GND" H 5150 4280 30  0001 C CNN
F 2 "" H 5150 4350 60  0000 C CNN
F 3 "" H 5150 4350 60  0000 C CNN
	1    5150 4350
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR013
U 1 1 5366689E
P 2000 1450
F 0 "#PWR013" H 2000 1550 30  0001 C CNN
F 1 "VCC" H 2000 1550 30  0000 C CNN
F 2 "" H 2000 1450 60  0000 C CNN
F 3 "" H 2000 1450 60  0000 C CNN
	1    2000 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 536668AD
P 1100 1950
F 0 "#PWR014" H 1100 1950 30  0001 C CNN
F 1 "GND" H 1100 1880 30  0001 C CNN
F 2 "" H 1100 1950 60  0000 C CNN
F 3 "" H 1100 1950 60  0000 C CNN
	1    1100 1950
	1    0    0    -1  
$EndComp
$Comp
L AVR-ISP-10 ISP1
U 1 1 537B8E9E
P 4200 1400
F 0 "ISP1" H 4000 1730 40  0000 C CNN
F 1 "AVR-ISP-10" H 4130 1100 40  0000 C CNN
F 2 "AVR-ISP-10" V 3580 1450 60  0001 C CNN
F 3 "~" H 4550 1400 60  0000 C CNN
	1    4200 1400
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR015
U 1 1 537B8EBF
P 4700 1100
F 0 "#PWR015" H 4700 1200 30  0001 C CNN
F 1 "VCC" H 4700 1200 30  0000 C CNN
F 2 "" H 4700 1100 60  0000 C CNN
F 3 "" H 4700 1100 60  0000 C CNN
	1    4700 1100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 537B8ECE
P 4700 1700
F 0 "#PWR016" H 4700 1700 30  0001 C CNN
F 1 "GND" H 4700 1630 30  0001 C CNN
F 2 "" H 4700 1700 60  0000 C CNN
F 3 "" H 4700 1700 60  0000 C CNN
	1    4700 1700
	1    0    0    -1  
$EndComp
Text Label 3650 1200 0    39   ~ 0
PB5
Text Label 3650 1600 0    39   ~ 0
PB6
Text Label 3650 1500 0    39   ~ 0
PB7
Text Label 3300 3750 0    39   ~ 0
RST
Text Label 3650 1400 0    39   ~ 0
RST
NoConn ~ 3800 1300
$Comp
L CRYSTAL X1
U 1 1 537BAD28
P 2700 4000
F 0 "X1" H 2700 4150 60  0000 C CNN
F 1 "8MHz" H 2700 3830 60  0000 C CNN
F 2 "~" H 2700 4000 60  0000 C CNN
F 3 "~" H 2700 4000 60  0000 C CNN
	1    2700 4000
	0    -1   -1   0   
$EndComp
$Comp
L CAP C1
U 1 1 537BAD4D
P 2400 3650
F 0 "C1" H 2440 3750 40  0000 L CNN
F 1 "22pF" H 2440 3540 40  0000 L CNN
F 2 "~" H 2438 3500 30  0000 C CNN
F 3 "~" H 2400 3650 60  0000 C CNN
	1    2400 3650
	0    -1   -1   0   
$EndComp
$Comp
L CAP C2
U 1 1 537BAD5C
P 2400 4300
F 0 "C2" H 2440 4400 40  0000 L CNN
F 1 "22pF" H 2440 4190 40  0000 L CNN
F 2 "~" H 2438 4150 30  0000 C CNN
F 3 "~" H 2400 4300 60  0000 C CNN
	1    2400 4300
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR017
U 1 1 537BAEE8
P 2100 4550
F 0 "#PWR017" H 2100 4550 30  0001 C CNN
F 1 "GND" H 2100 4480 30  0001 C CNN
F 2 "" H 2100 4550 60  0000 C CNN
F 3 "" H 2100 4550 60  0000 C CNN
	1    2100 4550
	1    0    0    -1  
$EndComp
$Comp
L RES R1
U 1 1 537BB20F
P 1850 3700
F 0 "R1" H 1850 3780 40  0000 C CNN
F 1 "10K" H 1850 3700 40  0000 C CNN
F 2 "~" H 1850 3630 30  0000 C CNN
F 3 "~" V 1850 3700 30  0000 C CNN
	1    1850 3700
	0    1    1    0   
$EndComp
$Comp
L CAP C3
U 1 1 537BB228
P 1850 4250
F 0 "C3" H 1890 4350 40  0000 L CNN
F 1 "100nF" H 1890 4140 40  0000 L CNN
F 2 "~" H 1888 4100 30  0000 C CNN
F 3 "~" H 1850 4250 60  0000 C CNN
	1    1850 4250
	1    0    0    -1  
$EndComp
Text Label 1400 4000 0    39   ~ 0
RST
Text Label 3250 4250 0    39   ~ 0
AVCC
Text Label 3250 4350 0    39   ~ 0
AREF
Text Label 1950 3400 0    39   ~ 0
AREF
$Comp
L IND L1
U 1 1 537BC8B2
P 850 3750
F 0 "L1" V 800 3850 40  0000 C CNN
F 1 "10uH" V 900 3900 40  0000 C CNN
F 2 "~" V 850 3750 60  0000 C CNN
F 3 "~" V 850 3750 60  0000 C CNN
	1    850  3750
	0    1    1    0   
$EndComp
$Comp
L CAP C4
U 1 1 537BC8C1
P 850 4350
F 0 "C4" H 890 4450 40  0000 L CNN
F 1 "100nF" H 890 4240 40  0000 L CNN
F 2 "~" H 888 4200 30  0000 C CNN
F 3 "~" H 850 4350 60  0000 C CNN
	1    850  4350
	1    0    0    -1  
$EndComp
Text Label 650  4100 0    39   ~ 0
AVCC
$Comp
L CAP C5
U 1 1 537BD46B
P 5250 4050
F 0 "C5" H 5290 4150 40  0000 L CNN
F 1 "100nF" H 5290 3940 40  0000 L CNN
F 2 "~" H 5288 3900 30  0000 C CNN
F 3 "~" H 5250 4050 60  0000 C CNN
	1    5250 4050
	1    0    0    -1  
$EndComp
$Comp
L CAP-POL C6
U 1 1 537BD47A
P 5600 4050
F 0 "C6" H 5640 4155 50  0000 L CNN
F 1 "100uF" H 5640 3905 50  0000 L CNN
F 2 "~" H 5600 4035 60  0000 C CNN
F 3 "~" H 5600 4035 60  0000 C CNN
	1    5600 4050
	1    0    0    -1  
$EndComp
$Comp
L CAP C7
U 1 1 537BD498
P 5950 4050
F 0 "C7" H 5990 4150 40  0000 L CNN
F 1 "100pF" H 5990 3940 40  0000 L CNN
F 2 "~" H 5988 3900 30  0000 C CNN
F 3 "~" H 5950 4050 60  0000 C CNN
	1    5950 4050
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR018
U 1 1 537BE3E6
P 1150 1550
F 0 "#PWR018" H 1150 1650 30  0001 C CNN
F 1 "VDD" H 1150 1660 30  0000 C CNN
F 2 "" H 1150 1550 60  0000 C CNN
F 3 "" H 1150 1550 60  0000 C CNN
	1    1150 1550
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG019
U 1 1 537BE494
P 1050 1450
F 0 "#FLG019" H 1050 1545 30  0001 C CNN
F 1 "PWR_FLAG" H 1050 1630 30  0000 C CNN
F 2 "" H 1050 1450 60  0000 C CNN
F 3 "" H 1050 1450 60  0000 C CNN
	1    1050 1450
	1    0    0    -1  
$EndComp
NoConn ~ 1900 1750
$Comp
L BUTTON PB1
U 1 1 537E3EEA
P 1450 4500
F 0 "PB1" H 1600 4610 50  0000 C CNN
F 1 "BUTTON" H 1450 4420 50  0000 C CNN
F 2 "~" H 1450 4500 60  0000 C CNN
F 3 "~" H 1450 4500 60  0000 C CNN
	1    1450 4500
	1    0    0    -1  
$EndComp
Connection ~ 1150 1650
Wire Wire Line
	1050 1450 1050 1650
Wire Wire Line
	1150 1650 1150 1550
Wire Wire Line
	1000 1650 1200 1650
Connection ~ 5250 4250
Connection ~ 5150 4250
Connection ~ 5600 4250
Connection ~ 5600 3850
Connection ~ 5250 3850
Connection ~ 5150 3850
Connection ~ 850  4100
Wire Wire Line
	850  4700 850  4550
Wire Wire Line
	850  4050 850  4150
Wire Wire Line
	850  3450 850  3400
Wire Wire Line
	1850 4700 850  4700
Connection ~ 1850 3400
Wire Wire Line
	3250 4350 3450 4350
Wire Wire Line
	3250 4250 3450 4250
Connection ~ 1850 4500
Connection ~ 1850 4000
Wire Wire Line
	1100 4000 1850 4000
Wire Wire Line
	1100 4500 1100 4000
Wire Wire Line
	1150 4500 1100 4500
Wire Wire Line
	2100 4500 1750 4500
Wire Wire Line
	1850 4450 1850 4700
Wire Wire Line
	1850 3950 1850 4050
Wire Wire Line
	1850 3450 1850 3400
Wire Wire Line
	850  3400 2450 3400
Connection ~ 2100 4300
Connection ~ 2100 4500
Wire Wire Line
	2100 3650 2100 4550
Wire Wire Line
	2200 4300 2100 4300
Connection ~ 2700 4300
Wire Wire Line
	2950 4300 2950 4050
Wire Wire Line
	2600 4300 2950 4300
Wire Wire Line
	2700 3650 2700 3750
Connection ~ 2200 3400
Wire Wire Line
	2950 4050 3450 4050
Connection ~ 2700 3650
Wire Wire Line
	2950 3650 2950 3950
Wire Wire Line
	2950 3950 3450 3950
Wire Wire Line
	2200 3650 2100 3650
Wire Wire Line
	2600 3650 2950 3650
Wire Wire Line
	3300 3750 3450 3750
Wire Wire Line
	3800 1600 3650 1600
Wire Wire Line
	3800 1500 3650 1500
Wire Wire Line
	3800 1400 3650 1400
Wire Wire Line
	3800 1200 3650 1200
Connection ~ 4700 1400
Wire Wire Line
	4600 1400 4700 1400
Connection ~ 4700 1500
Wire Wire Line
	4700 1500 4600 1500
Connection ~ 4700 1600
Wire Wire Line
	4600 1300 4700 1300
Wire Wire Line
	4700 1200 4700 1100
Wire Wire Line
	4600 1200 4700 1200
Wire Wire Line
	4700 1300 4700 1700
Wire Wire Line
	4600 1600 4700 1600
Wire Wire Line
	2000 1550 2000 1450
Wire Wire Line
	1100 1950 1100 1850
Wire Wire Line
	5150 3850 5150 3750
Wire Wire Line
	5150 4250 5150 4350
Connection ~ 5050 3850
Wire Wire Line
	4950 3750 5050 3750
Wire Wire Line
	4950 3850 5950 3850
Wire Wire Line
	5050 3750 5050 3950
Wire Wire Line
	5050 3950 4950 3950
Connection ~ 5050 4350
Wire Wire Line
	5050 4450 4950 4450
Connection ~ 5050 4250
Wire Wire Line
	5050 4350 4950 4350
Wire Wire Line
	4950 4250 5950 4250
Wire Wire Line
	5050 4150 5050 4450
Wire Wire Line
	4950 4150 5050 4150
Connection ~ 6100 2500
Wire Wire Line
	6200 2500 6200 2600
Connection ~ 2200 2500
Wire Wire Line
	2100 2300 2100 2500
Wire Wire Line
	6100 3400 6100 3500
Wire Wire Line
	6100 2500 6100 2600
Wire Wire Line
	5200 3300 5200 3400
Wire Wire Line
	5200 2400 5200 2500
Wire Wire Line
	3100 3400 3100 3500
Wire Wire Line
	2200 3300 2200 3400
Wire Wire Line
	3100 2500 3100 2600
Wire Wire Line
	2200 2400 2200 2500
Wire Wire Line
	3300 1950 3450 1950
Wire Wire Line
	3300 2050 3450 2050
Wire Wire Line
	3300 2150 3450 2150
Wire Wire Line
	3300 2250 3450 2250
Wire Wire Line
	3300 2350 3450 2350
Wire Wire Line
	3300 2450 3450 2450
Wire Wire Line
	3300 2550 3450 2550
Wire Wire Line
	3300 2650 3450 2650
Wire Wire Line
	3300 2850 3450 2850
Wire Wire Line
	3300 2950 3450 2950
Wire Wire Line
	3300 3050 3450 3050
Wire Wire Line
	3300 3150 3450 3150
Wire Wire Line
	3300 3250 3450 3250
Wire Wire Line
	3300 3350 3450 3350
Wire Wire Line
	3300 3450 3450 3450
Wire Wire Line
	3300 3550 3450 3550
Wire Wire Line
	4950 3550 5100 3550
Wire Wire Line
	4950 3450 5100 3450
Wire Wire Line
	4950 3350 5100 3350
Wire Wire Line
	4950 3250 5100 3250
Wire Wire Line
	4950 3150 5100 3150
Wire Wire Line
	4950 3050 5100 3050
Wire Wire Line
	4950 2950 5100 2950
Wire Wire Line
	4950 2850 5100 2850
Wire Wire Line
	4950 2650 5100 2650
Wire Wire Line
	4950 2550 5100 2550
Wire Wire Line
	4950 2450 5100 2450
Wire Wire Line
	4950 2350 5100 2350
Wire Wire Line
	4950 2250 5100 2250
Wire Wire Line
	4950 2150 5100 2150
Wire Wire Line
	4950 2050 5100 2050
Wire Wire Line
	4950 1950 5100 1950
Wire Wire Line
	2950 2100 3100 2100
Wire Wire Line
	2950 2200 3100 2200
Wire Wire Line
	2950 2300 3100 2300
Wire Wire Line
	2950 2400 3100 2400
Wire Wire Line
	2950 2500 3100 2500
Wire Wire Line
	2100 2500 2450 2500
Wire Wire Line
	2300 2400 2450 2400
Wire Wire Line
	2300 2300 2450 2300
Wire Wire Line
	2300 2200 2450 2200
Wire Wire Line
	2300 2100 2450 2100
Wire Wire Line
	2300 3000 2450 3000
Wire Wire Line
	2300 3100 2450 3100
Wire Wire Line
	2300 3200 2450 3200
Wire Wire Line
	2300 3300 2450 3300
Wire Wire Line
	2950 3400 3100 3400
Wire Wire Line
	2950 3300 3100 3300
Wire Wire Line
	2950 3200 3100 3200
Wire Wire Line
	2950 3100 3100 3100
Wire Wire Line
	2950 3000 3100 3000
Wire Wire Line
	5300 2100 5450 2100
Wire Wire Line
	5300 2200 5450 2200
Wire Wire Line
	5300 2300 5450 2300
Wire Wire Line
	5300 2400 5450 2400
Wire Wire Line
	5200 2500 5450 2500
Wire Wire Line
	5950 2500 6200 2500
Wire Wire Line
	5950 2400 6100 2400
Wire Wire Line
	5950 2300 6100 2300
Wire Wire Line
	5950 2200 6100 2200
Wire Wire Line
	5950 2100 6100 2100
Wire Wire Line
	5300 3000 5450 3000
Wire Wire Line
	5300 3100 5450 3100
Wire Wire Line
	5300 3200 5450 3200
Wire Wire Line
	5300 3300 5450 3300
Wire Wire Line
	5200 3400 5450 3400
Wire Wire Line
	5950 3400 6100 3400
Wire Wire Line
	5950 3300 6100 3300
Wire Wire Line
	5950 3200 6100 3200
Wire Wire Line
	5950 3100 6100 3100
Wire Wire Line
	5950 3000 6100 3000
Wire Wire Line
	1900 1550 2000 1550
Wire Wire Line
	1100 1850 1000 1850
Connection ~ 1050 1650
Wire Wire Line
	650  4100 850  4100
$Comp
L ATMEGA32A-TQFP44 IC1
U 1 1 54FEBA09
P 4200 3200
F 0 "IC1" H 3640 4590 40  0000 C CNN
F 1 "ATMEGA32A-TQFP44" H 3900 1800 40  0000 C CNN
F 2 "TQFP-44" H 4200 3200 30  0000 C CIN
F 3 "~" H 4200 2750 60  0000 C CNN
	1    4200 3200
	1    0    0    -1  
$EndComp
$Comp
L HEADER-5X2 P4
U 1 1 53664164
P 5700 3200
F 0 "P4" V 5650 3200 40  0000 C CNN
F 1 "HEADER-5X2" V 5750 3200 40  0000 C CNN
F 2 "~" H 5700 3200 60  0000 C CNN
F 3 "~" H 5700 3200 60  0000 C CNN
	1    5700 3200
	1    0    0    -1  
$EndComp
$Comp
L HEADER-2X1 P5
U 1 1 54FEBA18
P 750 1750
F 0 "P5" V 700 1750 40  0000 C CNN
F 1 "HEADER-2X1" V 800 1750 40  0000 C CNN
F 2 "~" H 750 1750 60  0000 C CNN
F 3 "~" H 750 1750 60  0000 C CNN
	1    750  1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2700 4250 2700 4300
$Comp
L LED D1
U 1 1 5504DCA9
P 700 2850
F 0 "D1" H 700 2940 50  0000 C CNN
F 1 "LED" H 710 2740 50  0000 C CNN
F 2 "~" H 700 2850 60  0000 C CNN
F 3 "~" H 700 2850 60  0000 C CNN
	1    700  2850
	0    1    1    0   
$EndComp
$Comp
L RES R2
U 1 1 5504DCBA
P 700 2400
F 0 "R2" H 700 2480 40  0000 C CNN
F 1 "270" H 700 2400 40  0000 C CNN
F 2 "~" H 700 2330 30  0000 C CNN
F 3 "~" V 700 2400 30  0000 C CNN
	1    700  2400
	0    -1   -1   0   
$EndComp
$Comp
L VCC #PWR020
U 1 1 5504DE82
P 700 2150
F 0 "#PWR020" H 700 2250 30  0001 C CNN
F 1 "VCC" H 700 2250 30  0000 C CNN
F 2 "" H 700 2150 60  0000 C CNN
F 3 "" H 700 2150 60  0000 C CNN
	1    700  2150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 5504DE9B
P 700 3050
F 0 "#PWR021" H 700 3050 30  0001 C CNN
F 1 "GND" H 700 2980 30  0001 C CNN
F 2 "" H 700 3050 60  0000 C CNN
F 3 "" H 700 3050 60  0000 C CNN
	1    700  3050
	1    0    0    -1  
$EndComp
$Comp
L SWITCH SW?
U 1 1 55057B6A
P 1550 1650
F 0 "SW?" H 1350 1750 40  0000 C CNN
F 1 "SWITCH" H 1400 1550 40  0000 C CNN
F 2 "~" H 1550 1650 60  0000 C CNN
F 3 "~" H 1550 1650 60  0000 C CNN
	1    1550 1650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
