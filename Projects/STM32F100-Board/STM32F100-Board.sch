EESchema Schematic File Version 2
LIBS:power
LIBS:Devices
LIBS:Components
LIBS:Connectors
LIBS:STM32F100-Board-cache
EELAYER 27 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date "9 apr 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5100 4650 0    39   ~ 0
SYS_XTAL1
Text Label 5900 4550 0    39   ~ 0
SYS_XTAL2
Text Label 5100 4850 0    39   ~ 0
RTC_XTAL1
Text Label 5900 4750 0    39   ~ 0
RTC_XTAL2
Text Label 3900 3800 0    39   ~ 0
BOOT0
Text Label 3900 2000 0    39   ~ 0
PB0
Text Label 3900 2100 0    39   ~ 0
PB1
Text Label 3900 2200 0    39   ~ 0
PB2
Text Label 3900 2300 0    39   ~ 0
PB3
Text Label 3900 2400 0    39   ~ 0
PB4
Text Label 3900 2500 0    39   ~ 0
PB5
Text Label 3900 2600 0    39   ~ 0
PB6
Text Label 3900 2700 0    39   ~ 0
PB7
Text Label 3900 2800 0    39   ~ 0
PB8
Text Label 3900 2900 0    39   ~ 0
PB9
Text Label 3900 3000 0    39   ~ 0
PB10
Text Label 3900 3100 0    39   ~ 0
PB11
Text Label 3900 3200 0    39   ~ 0
PB12
Text Label 3900 3300 0    39   ~ 0
PB13
Text Label 3900 3400 0    39   ~ 0
PB14
Text Label 3900 3500 0    39   ~ 0
PB15
Text Label 1750 3000 0    39   ~ 0
PA13
Text Label 1750 3100 0    39   ~ 0
PA14
Text Label 1750 3200 0    39   ~ 0
PA15
Text Label 1750 2900 0    39   ~ 0
PA12
Text Label 1750 2800 0    39   ~ 0
PA11
Text Label 1750 2700 0    39   ~ 0
PA10
Text Label 1800 2600 0    39   ~ 0
PA9
Text Label 1800 2500 0    39   ~ 0
PA8
Text Label 1800 2400 0    39   ~ 0
PA7
Text Label 1800 2300 0    39   ~ 0
PA6
Text Label 1800 2200 0    39   ~ 0
PA5
Text Label 1800 2100 0    39   ~ 0
PA4
Text Label 1800 2000 0    39   ~ 0
PA3
Text Label 5250 4950 0    39   ~ 0
VBAT
Text Label 5900 4850 0    39   ~ 0
PC13
Text Label 5300 4550 0    39   ~ 0
~RST
Text Label 5250 4150 0    39   ~ 0
VDDA
Text Label 5900 4050 0    39   ~ 0
PA0
Text Label 5300 4050 0    39   ~ 0
PA1
Text Label 5900 3950 0    39   ~ 0
PA2
Text Label 1800 1900 0    39   ~ 0
PA2
Text Label 1800 1800 0    39   ~ 0
PA1
Text Label 1800 1700 0    39   ~ 0
PA0
Text Label 3900 1800 0    39   ~ 0
VBAT
Text Label 1750 3450 0    39   ~ 0
PC13
Text Label 1600 3550 0    39   ~ 0
RTC_XTAL1
Text Label 1600 3650 0    39   ~ 0
RTC_XTAL2
Text Label 1600 3900 0    39   ~ 0
SYS_XTAL1
Text Label 1600 4000 0    39   ~ 0
SYS_XTAL2
Text Label 3900 3700 0    39   ~ 0
~RST
Wire Wire Line
	3900 2000 4000 2000
Wire Wire Line
	3900 2100 4000 2100
Wire Wire Line
	4000 2400 3900 2400
Wire Wire Line
	4000 2500 3900 2500
Wire Wire Line
	4000 2600 3900 2600
Wire Wire Line
	4000 2700 3900 2700
Wire Wire Line
	4000 2800 3900 2800
Wire Wire Line
	4000 2900 3900 2900
Wire Wire Line
	4050 3000 3900 3000
Wire Wire Line
	4050 3100 3900 3100
Wire Wire Line
	4050 3200 3900 3200
Wire Wire Line
	4050 3300 3900 3300
Wire Wire Line
	4050 3400 3900 3400
Wire Wire Line
	3900 3500 4050 3500
Wire Wire Line
	5400 4850 5100 4850
Wire Wire Line
	6200 4750 5900 4750
Wire Wire Line
	4100 3800 3900 3800
Wire Wire Line
	1750 3200 1900 3200
Wire Wire Line
	1900 2000 1800 2000
Wire Wire Line
	1800 2100 1900 2100
Wire Wire Line
	1900 2200 1800 2200
Wire Wire Line
	1800 2300 1900 2300
Wire Wire Line
	1900 2400 1800 2400
Wire Wire Line
	1800 2500 1900 2500
Wire Wire Line
	1900 2600 1800 2600
Wire Wire Line
	1900 2700 1750 2700
Wire Wire Line
	1750 2800 1900 2800
Wire Wire Line
	1900 2900 1750 2900
Wire Wire Line
	5250 4950 5400 4950
Wire Wire Line
	5900 4850 6050 4850
Wire Wire Line
	5300 4550 5400 4550
Wire Wire Line
	5100 4650 5400 4650
Wire Wire Line
	5900 4550 6200 4550
Wire Wire Line
	5900 3950 6000 3950
Wire Wire Line
	5900 4050 6000 4050
Wire Wire Line
	5250 4150 5400 4150
Wire Wire Line
	5400 4050 5300 4050
Wire Wire Line
	1900 1700 1800 1700
Wire Wire Line
	1800 1800 1900 1800
Wire Wire Line
	1900 1900 1800 1900
Wire Wire Line
	1750 3450 1900 3450
Wire Wire Line
	1900 3550 1600 3550
Wire Wire Line
	1900 3650 1600 3650
Wire Wire Line
	1600 3900 1900 3900
Wire Wire Line
	1600 4000 1900 4000
Wire Wire Line
	3900 3700 4000 3700
Wire Wire Line
	1750 3000 1900 3000
Wire Wire Line
	1750 3100 1900 3100
Wire Wire Line
	4000 2200 3900 2200
Wire Wire Line
	4000 2300 3900 2300
$Comp
L PWR_FLAG #FLG01
U 1 1 551F4251
P 4200 1800
F 0 "#FLG01" H 4200 1895 30  0001 C CNN
F 1 "PWR_FLAG" H 4200 1980 30  0000 C CNN
F 2 "" H 4200 1800 60  0000 C CNN
F 3 "" H 4200 1800 60  0000 C CNN
	1    4200 1800
	1    0    0    -1  
$EndComp
Text Label 3900 1700 0    39   ~ 0
VDDA
Wire Wire Line
	3900 1700 4050 1700
Wire Wire Line
	4050 1700 4050 1600
$Comp
L PWR_FLAG #FLG02
U 1 1 551FF2ED
P 4050 1600
F 0 "#FLG02" H 4050 1695 30  0001 C CNN
F 1 "PWR_FLAG" H 4050 1780 30  0000 C CNN
F 2 "" H 4050 1600 60  0000 C CNN
F 3 "" H 4050 1600 60  0000 C CNN
	1    4050 1600
	1    0    0    -1  
$EndComp
$Comp
L HEADER-13X2 P1
U 1 1 55232290
P 5650 4550
F 0 "P1" V 5600 4550 40  0000 C CNN
F 1 "HEADER-13X2" V 5700 4550 40  0000 C CNN
F 2 "~" H 5650 4600 60  0000 C CNN
F 3 "~" H 5650 4600 60  0000 C CNN
	1    5650 4550
	1    0    0    -1  
$EndComp
$Comp
L HEADER-13X2 P3
U 1 1 5523229F
P 7050 4550
F 0 "P3" V 7000 4550 40  0000 C CNN
F 1 "HEADER-13X2" V 7100 4550 40  0000 C CNN
F 2 "~" H 7050 4600 60  0000 C CNN
F 3 "~" H 7050 4600 60  0000 C CNN
	1    7050 4550
	1    0    0    -1  
$EndComp
$Comp
L HEADER-13X2 P2
U 1 1 552322AE
P 5650 6000
F 0 "P2" V 5600 6000 40  0000 C CNN
F 1 "HEADER-13X2" V 5700 6000 40  0000 C CNN
F 2 "~" H 5650 6050 60  0000 C CNN
F 3 "~" H 5650 6050 60  0000 C CNN
	1    5650 6000
	1    0    0    -1  
$EndComp
$Comp
L HEADER-13X2 P4
U 1 1 552322BD
P 7050 6000
F 0 "P4" V 7000 6000 40  0000 C CNN
F 1 "HEADER-13X2" V 7100 6000 40  0000 C CNN
F 2 "~" H 7050 6050 60  0000 C CNN
F 3 "~" H 7050 6050 60  0000 C CNN
	1    7050 6000
	1    0    0    -1  
$EndComp
Text Label 3000 1300 1    39   ~ 0
VDD4
Text Label 2900 1300 1    39   ~ 0
VDD5
Text Label 2800 1300 1    39   ~ 0
VDD6
Text Label 7450 3950 2    39   ~ 0
VDD4
Text Label 7450 5400 2    39   ~ 0
VDD5
Text Label 6050 5400 2    39   ~ 0
VDD6
Wire Wire Line
	7450 3950 7300 3950
Wire Wire Line
	7450 5400 7300 5400
Wire Wire Line
	6050 5400 5900 5400
NoConn ~ 5400 3950
NoConn ~ 6800 3950
NoConn ~ 6800 5400
NoConn ~ 5400 5400
Text Label 5250 4250 0    39   ~ 0
VSSA
Text Label 2900 4550 1    39   ~ 0
VSS3
Text Label 2800 4550 1    39   ~ 0
VSS4
Wire Wire Line
	5400 4250 5250 4250
Text Label 3900 4000 0    39   ~ 0
VSSA
Wire Wire Line
	3900 4000 4050 4000
Text Label 3000 4550 1    39   ~ 0
VSS2
Text Label 6800 5500 2    39   ~ 0
VSS3
Text Label 5400 5500 2    39   ~ 0
VSS4
Wire Wire Line
	6800 5500 6650 5500
Wire Wire Line
	5250 5500 5400 5500
Text Label 6800 5150 2    39   ~ 0
VSS2
Wire Wire Line
	6800 5150 6650 5150
Wire Wire Line
	7300 5150 7400 5150
Text Label 7300 5150 0    39   ~ 0
PA3
Text Label 7300 4850 0    39   ~ 0
PA7
Text Label 6700 4950 0    39   ~ 0
PA6
Text Label 7300 4950 0    39   ~ 0
PA5
Text Label 6700 5050 0    39   ~ 0
PA4
Wire Wire Line
	6700 5050 6800 5050
Wire Wire Line
	7400 4950 7300 4950
Wire Wire Line
	6700 4950 6800 4950
Wire Wire Line
	7400 4850 7300 4850
Text Label 6700 4750 0    39   ~ 0
PB0
Text Label 7300 4650 0    39   ~ 0
PB1
Text Label 6700 4650 0    39   ~ 0
PB2
Wire Wire Line
	6700 4750 6800 4750
Wire Wire Line
	7300 4650 7400 4650
Wire Wire Line
	6800 4650 6700 4650
Text Label 6650 4150 0    39   ~ 0
PB10
Text Label 7300 4050 0    39   ~ 0
PB11
Wire Wire Line
	6800 4150 6650 4150
Wire Wire Line
	7450 4050 7300 4050
Text Label 7300 6600 0    39   ~ 0
PB12
Text Label 6650 6600 0    39   ~ 0
PB13
Text Label 7300 6500 0    39   ~ 0
PB14
Text Label 6650 6500 0    39   ~ 0
PB15
Wire Wire Line
	7450 6600 7300 6600
Wire Wire Line
	6800 6600 6650 6600
Wire Wire Line
	7450 6500 7300 6500
Wire Wire Line
	6650 6500 6800 6500
Text Label 6650 5600 0    39   ~ 0
PA13
Text Label 7300 5600 0    39   ~ 0
PA12
Text Label 6650 5700 0    39   ~ 0
PA11
Text Label 7300 5700 0    39   ~ 0
PA10
Text Label 6700 5800 0    39   ~ 0
PA9
Text Label 7300 5800 0    39   ~ 0
PA8
Wire Wire Line
	7300 5800 7400 5800
Wire Wire Line
	6800 5800 6700 5800
Wire Wire Line
	7450 5700 7300 5700
Wire Wire Line
	6650 5700 6800 5700
Wire Wire Line
	7450 5600 7300 5600
Wire Wire Line
	6650 5600 6800 5600
Text Label 5900 6600 0    39   ~ 0
PA14
Text Label 5250 6600 0    39   ~ 0
PA15
Wire Wire Line
	5250 6600 5400 6600
Wire Wire Line
	5900 6600 6050 6600
Text Label 5300 6000 0    39   ~ 0
PB3
Text Label 5900 5900 0    39   ~ 0
PB4
Text Label 5300 5900 0    39   ~ 0
PB5
Text Label 5900 5800 0    39   ~ 0
PB6
Text Label 5300 5800 0    39   ~ 0
PB7
Text Label 5300 5700 0    39   ~ 0
PB8
Text Label 5900 5600 0    39   ~ 0
PB9
Wire Wire Line
	6000 5900 5900 5900
Wire Wire Line
	5400 5900 5300 5900
Wire Wire Line
	6000 5800 5900 5800
Wire Wire Line
	5400 5800 5300 5800
Wire Wire Line
	5400 5700 5300 5700
Wire Wire Line
	6000 5600 5900 5600
Wire Wire Line
	5400 6000 5300 6000
Text Label 5900 5700 0    39   ~ 0
BOOT0
Wire Wire Line
	6100 5700 5900 5700
$Comp
L PWR_FLAG #FLG03
U 1 1 552331E1
P 2800 1150
F 0 "#FLG03" H 2800 1245 30  0001 C CNN
F 1 "PWR_FLAG" H 2800 1330 30  0000 C CNN
F 2 "" H 2800 1150 60  0000 C CNN
F 3 "" H 2800 1150 60  0000 C CNN
	1    2800 1150
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 552331FF
P 2900 1050
F 0 "#FLG04" H 2900 1145 30  0001 C CNN
F 1 "PWR_FLAG" H 2900 1230 30  0000 C CNN
F 2 "" H 2900 1050 60  0000 C CNN
F 3 "" H 2900 1050 60  0000 C CNN
	1    2900 1050
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG05
U 1 1 55233BE4
P 3000 4550
F 0 "#FLG05" H 3000 4645 30  0001 C CNN
F 1 "PWR_FLAG" H 3000 4730 30  0000 C CNN
F 2 "" H 3000 4550 60  0000 C CNN
F 3 "" H 3000 4550 60  0000 C CNN
	1    3000 4550
	0    1    1    0   
$EndComp
$Comp
L PWR_FLAG #FLG06
U 1 1 55233BF3
P 2800 4550
F 0 "#FLG06" H 2800 4645 30  0001 C CNN
F 1 "PWR_FLAG" H 2800 4730 30  0000 C CNN
F 2 "" H 2800 4550 60  0000 C CNN
F 3 "" H 2800 4550 60  0000 C CNN
	1    2800 4550
	0    -1   -1   0   
$EndComp
$Comp
L PWR_FLAG #FLG07
U 1 1 55233C02
P 2900 4650
F 0 "#FLG07" H 2900 4745 30  0001 C CNN
F 1 "PWR_FLAG" H 2900 4830 30  0000 C CNN
F 2 "" H 2900 4650 60  0000 C CNN
F 3 "" H 2900 4650 60  0000 C CNN
	1    2900 4650
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG08
U 1 1 55233E5B
P 4050 4000
F 0 "#FLG08" H 4050 4095 30  0001 C CNN
F 1 "PWR_FLAG" H 4050 4180 30  0000 C CNN
F 2 "" H 4050 4000 60  0000 C CNN
F 3 "" H 4050 4000 60  0000 C CNN
	1    4050 4000
	0    1    1    0   
$EndComp
$Comp
L STM32F100C4-LQFP48 IC1
U 1 1 55234265
P 2900 2850
F 0 "IC1" H 2100 4300 40  0000 C CNN
F 1 "STM32F100C4-LQFP48" H 2400 1400 40  0000 C CNN
F 2 "LQFP-48" H 2900 2850 30  0000 C CIN
F 3 "~" H 2900 2300 60  0000 C CNN
	1    2900 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 1800 3900 1800
Wire Wire Line
	3000 4550 3000 4400
Wire Wire Line
	2800 1150 2800 1300
$Comp
L PWR_FLAG #FLG09
U 1 1 55233753
P 3000 1150
F 0 "#FLG09" H 3000 1245 30  0001 C CNN
F 1 "PWR_FLAG" H 3000 1330 30  0000 C CNN
F 2 "" H 3000 1150 60  0000 C CNN
F 3 "" H 3000 1150 60  0000 C CNN
	1    3000 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 1150 3000 1300
Wire Wire Line
	2900 4400 2900 4650
Wire Wire Line
	2900 1050 2900 1300
Wire Wire Line
	2800 4550 2800 4400
NoConn ~ 7300 4150
NoConn ~ 7300 4250
NoConn ~ 7300 4350
NoConn ~ 7300 4450
NoConn ~ 7300 4550
NoConn ~ 6800 4550
NoConn ~ 6800 4450
NoConn ~ 6800 4350
NoConn ~ 6800 4250
NoConn ~ 6800 4050
NoConn ~ 5900 4150
NoConn ~ 5900 4250
NoConn ~ 5900 4350
NoConn ~ 5900 4450
NoConn ~ 5400 4450
NoConn ~ 5400 4350
NoConn ~ 5400 4750
NoConn ~ 5900 4650
NoConn ~ 5900 4950
NoConn ~ 5900 5050
NoConn ~ 5900 5150
NoConn ~ 5400 5150
NoConn ~ 5400 5050
NoConn ~ 5400 5600
NoConn ~ 5900 5500
NoConn ~ 5900 6000
NoConn ~ 5900 6100
NoConn ~ 5900 6200
NoConn ~ 5400 6200
NoConn ~ 5400 6100
NoConn ~ 5400 6300
NoConn ~ 5900 6300
NoConn ~ 5900 6400
NoConn ~ 5900 6500
NoConn ~ 5400 6500
NoConn ~ 5400 6400
NoConn ~ 6800 6400
NoConn ~ 6800 6300
NoConn ~ 6800 6200
NoConn ~ 6800 6100
NoConn ~ 6800 6000
NoConn ~ 6800 5900
NoConn ~ 7300 5900
NoConn ~ 7300 6000
NoConn ~ 7300 6100
NoConn ~ 7300 6200
NoConn ~ 7300 6300
NoConn ~ 7300 6400
NoConn ~ 7300 5500
NoConn ~ 7300 5050
NoConn ~ 6800 4850
NoConn ~ 7300 4750
$EndSCHEMATC
