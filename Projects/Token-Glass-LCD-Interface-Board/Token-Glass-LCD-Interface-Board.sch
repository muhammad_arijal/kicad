EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Components
LIBS:Connectors
LIBS:Devices
LIBS:Token-Glass-LCD-Interface-Board-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "10 mar 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 800  1950 1    39   ~ 0
LCD0
Text Label 900  1950 1    39   ~ 0
LCD1
Text Label 1000 1950 1    39   ~ 0
LCD2
Text Label 1100 1950 1    39   ~ 0
LCD3
Text Label 1200 1950 1    39   ~ 0
LCD4
Text Label 1300 1950 1    39   ~ 0
LCD5
Text Label 1400 1950 1    39   ~ 0
LCD6
Text Label 1500 1950 1    39   ~ 0
LCD7
Text Label 1600 1950 1    39   ~ 0
LCD8
Text Label 1700 1950 1    39   ~ 0
LCD9
Text Label 1800 2000 1    39   ~ 0
LCD10
Text Label 1900 2000 1    39   ~ 0
LCD11
Text Label 2000 2000 1    39   ~ 0
LCD12
Text Label 2100 2000 1    39   ~ 0
LCD13
Text Label 2200 2000 1    39   ~ 0
LCD14
Text Label 2300 2000 1    39   ~ 0
LCD15
Text Label 2400 2000 1    39   ~ 0
LCD16
Text Label 2500 2000 1    39   ~ 0
LCD17
Text Label 800  800  1    39   ~ 0
LCD18
Text Label 900  800  1    39   ~ 0
LCD19
Text Label 1000 800  1    39   ~ 0
LCD20
Text Label 1100 800  1    39   ~ 0
LCD21
Text Label 1200 800  1    39   ~ 0
LCD22
Text Label 1300 800  1    39   ~ 0
LCD23
Text Label 1400 800  1    39   ~ 0
LCD24
Text Label 1500 800  1    39   ~ 0
LCD25
Text Label 1600 800  1    39   ~ 0
LCD26
Text Label 1700 800  1    39   ~ 0
LCD27
Text Label 1800 800  1    39   ~ 0
LCD28
Text Label 1900 800  1    39   ~ 0
LCD29
Text Label 2000 800  1    39   ~ 0
LCD30
Text Label 2100 800  1    39   ~ 0
LCD31
Text Label 2200 800  1    39   ~ 0
LCD32
Text Label 2300 800  1    39   ~ 0
LCD33
Text Label 2400 800  1    39   ~ 0
LCD34
Text Label 2500 800  1    39   ~ 0
LCD35
Wire Wire Line
	800  600  800  800 
Wire Wire Line
	900  800  900  600 
Wire Wire Line
	1000 800  1000 600 
Wire Wire Line
	1100 800  1100 600 
Wire Wire Line
	1200 800  1200 600 
Wire Wire Line
	1300 800  1300 600 
Wire Wire Line
	1400 800  1400 600 
Wire Wire Line
	1500 800  1500 600 
Wire Wire Line
	1600 800  1600 600 
Wire Wire Line
	1700 800  1700 600 
Wire Wire Line
	1800 800  1800 600 
Wire Wire Line
	1900 800  1900 600 
Wire Wire Line
	2000 800  2000 600 
Wire Wire Line
	2100 800  2100 600 
Wire Wire Line
	2200 800  2200 600 
Wire Wire Line
	2300 800  2300 600 
Wire Wire Line
	2400 800  2400 600 
Wire Wire Line
	2500 800  2500 600 
Wire Wire Line
	800  1950 800  1800
Wire Wire Line
	900  1800 900  1950
Wire Wire Line
	1000 1950 1000 1800
Wire Wire Line
	1100 1800 1100 1950
Wire Wire Line
	1200 1950 1200 1800
Wire Wire Line
	1300 1800 1300 1950
Wire Wire Line
	1400 1950 1400 1800
Wire Wire Line
	1500 1950 1500 1800
Wire Wire Line
	1600 1800 1600 1950
Wire Wire Line
	1700 1950 1700 1800
Wire Wire Line
	1800 1800 1800 2000
Wire Wire Line
	1900 2000 1900 1800
Wire Wire Line
	2000 1800 2000 2000
Wire Wire Line
	2100 2000 2100 1800
Wire Wire Line
	2200 1800 2200 2000
Wire Wire Line
	2300 2000 2300 1800
Wire Wire Line
	2400 1800 2400 2000
Wire Wire Line
	2500 2000 2500 1800
Text Label 800  2550 1    39   ~ 0
LCD0
Text Label 900  2550 1    39   ~ 0
LCD1
Text Label 1000 2550 1    39   ~ 0
LCD2
Text Label 1100 2550 1    39   ~ 0
LCD3
Text Label 1200 2550 1    39   ~ 0
LCD4
Text Label 1300 2550 1    39   ~ 0
LCD5
Text Label 1400 2550 1    39   ~ 0
LCD6
Text Label 1500 2550 1    39   ~ 0
LCD7
Text Label 1600 2550 1    39   ~ 0
LCD8
Text Label 1700 2550 1    39   ~ 0
LCD9
Text Label 1800 2600 1    39   ~ 0
LCD10
Text Label 1900 2600 1    39   ~ 0
LCD11
Text Label 2000 2600 1    39   ~ 0
LCD12
Text Label 2100 2600 1    39   ~ 0
LCD13
Text Label 2200 2600 1    39   ~ 0
LCD14
Text Label 2300 2600 1    39   ~ 0
LCD15
Text Label 2400 2600 1    39   ~ 0
LCD16
Text Label 2500 2600 1    39   ~ 0
LCD17
Wire Wire Line
	800  2550 800  2400
Wire Wire Line
	900  2400 900  2550
Wire Wire Line
	1000 2550 1000 2400
Wire Wire Line
	1100 2400 1100 2550
Wire Wire Line
	1200 2550 1200 2400
Wire Wire Line
	1300 2400 1300 2550
Wire Wire Line
	1400 2550 1400 2400
Wire Wire Line
	1500 2550 1500 2400
Wire Wire Line
	1600 2400 1600 2550
Wire Wire Line
	1700 2550 1700 2400
Wire Wire Line
	1800 2400 1800 2600
Wire Wire Line
	1900 2600 1900 2400
Wire Wire Line
	2000 2400 2000 2600
Wire Wire Line
	2100 2600 2100 2400
Wire Wire Line
	2200 2400 2200 2600
Wire Wire Line
	2300 2600 2300 2400
Wire Wire Line
	2400 2400 2400 2600
Wire Wire Line
	2500 2600 2500 2400
Text Label 800  4400 1    39   ~ 0
LCD18
Text Label 900  4400 1    39   ~ 0
LCD19
Text Label 1000 4400 1    39   ~ 0
LCD20
Text Label 1100 4400 1    39   ~ 0
LCD21
Text Label 1200 4400 1    39   ~ 0
LCD22
Text Label 1300 4400 1    39   ~ 0
LCD23
Text Label 1400 4400 1    39   ~ 0
LCD24
Text Label 1500 4400 1    39   ~ 0
LCD25
Text Label 1600 4400 1    39   ~ 0
LCD26
Text Label 1700 4400 1    39   ~ 0
LCD27
Text Label 1800 4400 1    39   ~ 0
LCD28
Text Label 1900 4400 1    39   ~ 0
LCD29
Text Label 2000 4400 1    39   ~ 0
LCD30
Text Label 2100 4400 1    39   ~ 0
LCD31
Text Label 2200 4400 1    39   ~ 0
LCD32
Text Label 2300 4400 1    39   ~ 0
LCD33
Text Label 2400 4400 1    39   ~ 0
LCD34
Text Label 2500 4400 1    39   ~ 0
LCD35
Wire Wire Line
	800  4200 800  4400
Wire Wire Line
	900  4400 900  4200
Wire Wire Line
	1000 4400 1000 4200
Wire Wire Line
	1100 4400 1100 4200
Wire Wire Line
	1200 4400 1200 4200
Wire Wire Line
	1300 4400 1300 4200
Wire Wire Line
	1400 4400 1400 4200
Wire Wire Line
	1500 4400 1500 4200
Wire Wire Line
	1600 4400 1600 4200
Wire Wire Line
	1700 4400 1700 4200
Wire Wire Line
	1800 4400 1800 4200
Wire Wire Line
	1900 4400 1900 4200
Wire Wire Line
	2000 4400 2000 4200
Wire Wire Line
	2100 4400 2100 4200
Wire Wire Line
	2200 4400 2200 4200
Wire Wire Line
	2300 4400 2300 4200
Wire Wire Line
	2400 4400 2400 4200
Wire Wire Line
	2500 4400 2500 4200
Text Label 800  3750 1    39   ~ 0
LCD0
Text Label 900  3750 1    39   ~ 0
LCD1
Text Label 1000 3750 1    39   ~ 0
LCD2
Text Label 1100 3750 1    39   ~ 0
LCD3
Text Label 1200 3750 1    39   ~ 0
LCD4
Text Label 1300 3750 1    39   ~ 0
LCD5
Text Label 1400 3750 1    39   ~ 0
LCD6
Text Label 1500 3750 1    39   ~ 0
LCD7
Text Label 1600 3750 1    39   ~ 0
LCD8
Text Label 1700 3750 1    39   ~ 0
LCD9
Text Label 1800 3800 1    39   ~ 0
LCD10
Text Label 1900 3800 1    39   ~ 0
LCD11
Text Label 2000 3800 1    39   ~ 0
LCD12
Text Label 2100 3800 1    39   ~ 0
LCD13
Text Label 2200 3800 1    39   ~ 0
LCD14
Text Label 2300 3800 1    39   ~ 0
LCD15
Text Label 2400 3800 1    39   ~ 0
LCD16
Text Label 2500 3800 1    39   ~ 0
LCD17
Wire Wire Line
	800  3750 800  3600
Wire Wire Line
	900  3600 900  3750
Wire Wire Line
	1000 3750 1000 3600
Wire Wire Line
	1100 3600 1100 3750
Wire Wire Line
	1200 3750 1200 3600
Wire Wire Line
	1300 3600 1300 3750
Wire Wire Line
	1400 3750 1400 3600
Wire Wire Line
	1500 3750 1500 3600
Wire Wire Line
	1600 3600 1600 3750
Wire Wire Line
	1700 3750 1700 3600
Wire Wire Line
	1800 3600 1800 3800
Wire Wire Line
	1900 3800 1900 3600
Wire Wire Line
	2000 3600 2000 3800
Wire Wire Line
	2100 3800 2100 3600
Wire Wire Line
	2200 3600 2200 3800
Wire Wire Line
	2300 3800 2300 3600
Wire Wire Line
	2400 3600 2400 3800
Wire Wire Line
	2500 3800 2500 3600
Text Label 800  3200 1    39   ~ 0
LCD18
Text Label 900  3200 1    39   ~ 0
LCD19
Text Label 1000 3200 1    39   ~ 0
LCD20
Text Label 1100 3200 1    39   ~ 0
LCD21
Text Label 1200 3200 1    39   ~ 0
LCD22
Text Label 1300 3200 1    39   ~ 0
LCD23
Text Label 1400 3200 1    39   ~ 0
LCD24
Text Label 1500 3200 1    39   ~ 0
LCD25
Text Label 1600 3200 1    39   ~ 0
LCD26
Text Label 1700 3200 1    39   ~ 0
LCD27
Text Label 1800 3200 1    39   ~ 0
LCD28
Text Label 1900 3200 1    39   ~ 0
LCD29
Text Label 2000 3200 1    39   ~ 0
LCD30
Text Label 2100 3200 1    39   ~ 0
LCD31
Text Label 2200 3200 1    39   ~ 0
LCD32
Text Label 2300 3200 1    39   ~ 0
LCD33
Text Label 2400 3200 1    39   ~ 0
LCD34
Text Label 2500 3200 1    39   ~ 0
LCD35
Wire Wire Line
	800  3000 800  3200
Wire Wire Line
	900  3200 900  3000
Wire Wire Line
	1000 3200 1000 3000
Wire Wire Line
	1100 3200 1100 3000
Wire Wire Line
	1200 3200 1200 3000
Wire Wire Line
	1300 3200 1300 3000
Wire Wire Line
	1400 3200 1400 3000
Wire Wire Line
	1500 3200 1500 3000
Wire Wire Line
	1600 3200 1600 3000
Wire Wire Line
	1700 3200 1700 3000
Wire Wire Line
	1800 3200 1800 3000
Wire Wire Line
	1900 3200 1900 3000
Wire Wire Line
	2000 3200 2000 3000
Wire Wire Line
	2100 3200 2100 3000
Wire Wire Line
	2200 3200 2200 3000
Wire Wire Line
	2300 3200 2300 3000
Wire Wire Line
	2400 3200 2400 3000
Wire Wire Line
	2500 3200 2500 3000
$Comp
L LCD-TOKEN LCD1
U 1 1 54FEB3AF
P 1650 1300
F 0 "LCD1" V 600 1025 40  0000 C CNN
F 1 "LCD-TOKEN" V 2700 1125 40  0000 C CNN
F 2 "LCD-TOKEN" V 2700 1125 40  0000 C CNN
F 3 "~" H 1775 1653 60  0000 C CNN
	1    1650 1300
	1    0    0    -1  
$EndComp
$Comp
L HEADER-18X1 P1
U 1 1 54FEB3BE
P 1650 2150
F 0 "P1" V 1600 2150 40  0000 C CNN
F 1 "HEADER-18X1" V 1700 2150 40  0000 C CNN
F 2 "~" H 1650 2550 60  0000 C CNN
F 3 "~" H 1650 2550 60  0000 C CNN
	1    1650 2150
	0    -1   -1   0   
$EndComp
$Comp
L HEADER-18X1 P2
U 1 1 54FEB3CD
P 1650 2750
F 0 "P2" V 1600 2750 40  0000 C CNN
F 1 "HEADER-18X1" V 1700 2750 40  0000 C CNN
F 2 "~" H 1650 3150 60  0000 C CNN
F 3 "~" H 1650 3150 60  0000 C CNN
	1    1650 2750
	0    -1   -1   0   
$EndComp
$Comp
L HEADER-18X1 P3
U 1 1 54FEB3DC
P 1650 3350
F 0 "P3" V 1600 3350 40  0000 C CNN
F 1 "HEADER-18X1" V 1700 3350 40  0000 C CNN
F 2 "~" H 1650 3750 60  0000 C CNN
F 3 "~" H 1650 3750 60  0000 C CNN
	1    1650 3350
	0    -1   -1   0   
$EndComp
$Comp
L HEADER-18X1 P4
U 1 1 54FEB3EB
P 1650 3950
F 0 "P4" V 1600 3950 40  0000 C CNN
F 1 "HEADER-18X1" V 1700 3950 40  0000 C CNN
F 2 "~" H 1650 4350 60  0000 C CNN
F 3 "~" H 1650 4350 60  0000 C CNN
	1    1650 3950
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
