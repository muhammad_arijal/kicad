EESchema Schematic File Version 2
LIBS:power
LIBS:Components
LIBS:Devices
LIBS:Connectors
LIBS:Token-Sub-Board-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "15 aug 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 600  2900 0    39   ~ 0
LCD1
Text Label 600  3000 0    39   ~ 0
LCD2
Text Label 600  3100 0    39   ~ 0
LCD3
Text Label 600  3200 0    39   ~ 0
LCD4
Text Label 600  3300 0    39   ~ 0
LCD5
Text Label 600  3400 0    39   ~ 0
LCD6
Text Label 600  3500 0    39   ~ 0
LCD7
Text Label 600  3600 0    39   ~ 0
LCD8
Text Label 600  3700 0    39   ~ 0
LCD9
Text Label 550  3800 0    39   ~ 0
LCD10
Text Label 550  3900 0    39   ~ 0
LCD11
Text Label 550  4000 0    39   ~ 0
LCD12
Text Label 550  4100 0    39   ~ 0
LCD13
Text Label 550  4200 0    39   ~ 0
LCD14
Text Label 550  4300 0    39   ~ 0
LCD15
Text Label 550  4400 0    39   ~ 0
LCD16
Text Label 1750 4300 0    39   ~ 0
LCD35
Text Label 550  4700 0    39   ~ 0
LCD19
Text Label 1750 2800 0    39   ~ 0
LCD20
Text Label 1750 2900 0    39   ~ 0
LCD21
Text Label 1750 3000 0    39   ~ 0
LCD22
Text Label 1750 3100 0    39   ~ 0
LCD23
Text Label 1750 3200 0    39   ~ 0
LCD24
Text Label 1750 3300 0    39   ~ 0
LCD25
Text Label 1750 3400 0    39   ~ 0
LCD26
Text Label 1750 3500 0    39   ~ 0
LCD27
Text Label 1750 3600 0    39   ~ 0
LCD28
Text Label 1750 3700 0    39   ~ 0
LCD29
Text Label 1750 3800 0    39   ~ 0
LCD30
Text Label 1750 3900 0    39   ~ 0
LCD31
Text Label 1750 4000 0    39   ~ 0
LCD32
Text Label 1750 4100 0    39   ~ 0
LCD33
Text Label 1750 4200 0    39   ~ 0
LCD34
Text Label 600  2800 0    39   ~ 0
LCD0
Text Label 550  4500 0    39   ~ 0
LCD17
Text Label 550  4600 0    39   ~ 0
LCD18
$Comp
L HEADER-10X2 P1
U 1 1 55620401
P 3950 1650
F 0 "P1" V 3900 1650 40  0000 C CNN
F 1 "HEADER-10X2" V 4000 1650 40  0000 C CNN
F 2 "~" H 3950 1550 60  0000 C CNN
F 3 "~" H 3950 1550 60  0000 C CNN
	1    3950 1650
	1    0    0    -1  
$EndComp
$Comp
L HEADER-10X2 P2
U 1 1 55620410
P 3950 2800
F 0 "P2" V 3900 2800 40  0000 C CNN
F 1 "HEADER-10X2" V 4000 2800 40  0000 C CNN
F 2 "~" H 3950 2700 60  0000 C CNN
F 3 "~" H 3950 2700 60  0000 C CNN
	1    3950 2800
	1    0    0    -1  
$EndComp
Text Label 4200 2350 0    39   ~ 0
LCD0
Text Label 3550 2350 0    40   ~ 0
LCD1
Text Label 4200 2450 0    39   ~ 0
LCD2
Text Label 4200 2550 0    39   ~ 0
LCD4
Text Label 4200 2650 0    39   ~ 0
LCD6
Text Label 4200 2750 0    39   ~ 0
LCD8
Text Label 4200 2850 0    39   ~ 0
LCD10
Text Label 4200 2950 0    39   ~ 0
LCD12
Text Label 4200 3050 0    39   ~ 0
LCD14
Text Label 4200 3150 0    39   ~ 0
LCD16
Text Label 4200 3250 0    39   ~ 0
LCD18
Text Label 3550 2450 0    39   ~ 0
LCD3
Text Label 3550 2550 0    39   ~ 0
LCD5
Text Label 3550 2650 0    39   ~ 0
LCD7
Text Label 3550 2750 0    39   ~ 0
LCD9
Text Label 3500 2850 0    39   ~ 0
LCD11
Text Label 3500 2950 0    39   ~ 0
LCD13
Text Label 3500 3050 0    39   ~ 0
LCD15
Text Label 3500 3150 0    39   ~ 0
LCD17
Text Label 3500 3250 0    39   ~ 0
LCD19
Text Label 3500 1200 0    39   ~ 0
LCD21
Text Label 3500 1300 0    39   ~ 0
LCD23
Text Label 3500 1400 0    39   ~ 0
LCD25
Text Label 3500 1500 0    39   ~ 0
LCD27
Text Label 3500 1600 0    39   ~ 0
LCD29
Text Label 4200 1200 0    39   ~ 0
LCD20
Text Label 4200 1300 0    39   ~ 0
LCD22
Text Label 4200 1400 0    39   ~ 0
LCD24
Text Label 4200 1500 0    39   ~ 0
LCD26
Text Label 4200 1600 0    39   ~ 0
LCD28
Text Label 4200 1700 0    39   ~ 0
LCD30
Text Label 4200 1800 0    39   ~ 0
LCD32
Text Label 4200 1900 0    39   ~ 0
LCD34
Text Label 4200 2000 0    39   ~ 0
LCD36
Text Label 4200 2100 0    39   ~ 0
LCD38
Text Label 3500 1700 0    39   ~ 0
LCD31
Text Label 3500 1800 0    39   ~ 0
LCD33
Text Label 3500 1900 0    39   ~ 0
LCD35
Text Label 3500 2000 0    39   ~ 0
LCD37
Text Label 3500 2100 0    39   ~ 0
LCD39
Text Label 1750 4700 0    39   ~ 0
LCD39
Text Label 1750 4400 0    39   ~ 0
LCD36
Text Label 1750 4500 0    39   ~ 0
LCD37
Text Label 1750 4600 0    39   ~ 0
LCD38
$Comp
L LCD-TOKEN LCD1
U 1 1 556233B6
P 1250 3750
F 0 "LCD1" V 100 3475 40  0000 C CNN
F 1 "LCD-TOKEN" V 2400 3575 40  0000 C CNN
F 2 "LCD-TOKEN" V 2400 3575 40  0000 C CNN
F 3 "~" H 1375 4103 60  0000 C CNN
	1    1250 3750
	0    1    -1   0   
$EndComp
Wire Wire Line
	600  2900 750  2900
Wire Wire Line
	600  3000 750  3000
Wire Wire Line
	750  3100 600  3100
Wire Wire Line
	600  3200 750  3200
Wire Wire Line
	750  3300 600  3300
Wire Wire Line
	600  3400 750  3400
Wire Wire Line
	750  3500 600  3500
Wire Wire Line
	600  3600 750  3600
Wire Wire Line
	750  3700 600  3700
Wire Wire Line
	550  3800 750  3800
Wire Wire Line
	550  3900 750  3900
Wire Wire Line
	550  4000 750  4000
Wire Wire Line
	550  4100 750  4100
Wire Wire Line
	550  4200 750  4200
Wire Wire Line
	550  4300 750  4300
Wire Wire Line
	550  4400 750  4400
Wire Wire Line
	1750 4300 1950 4300
Wire Wire Line
	1750 4200 1950 4200
Wire Wire Line
	1950 4100 1750 4100
Wire Wire Line
	1750 4000 1950 4000
Wire Wire Line
	1950 3900 1750 3900
Wire Wire Line
	1750 3800 1950 3800
Wire Wire Line
	1950 3700 1750 3700
Wire Wire Line
	1750 3600 1950 3600
Wire Wire Line
	1950 3500 1750 3500
Wire Wire Line
	1750 3400 1950 3400
Wire Wire Line
	1950 3300 1750 3300
Wire Wire Line
	1750 3200 1950 3200
Wire Wire Line
	1950 3100 1750 3100
Wire Wire Line
	1750 3000 1950 3000
Wire Wire Line
	1750 2900 1950 2900
Wire Wire Line
	1950 2800 1750 2800
Wire Wire Line
	550  4700 750  4700
Wire Wire Line
	550  4500 750  4500
Wire Wire Line
	600  2800 750  2800
Wire Wire Line
	550  4600 750  4600
Wire Wire Line
	3550 2350 3700 2350
Wire Wire Line
	4200 2350 4350 2350
Wire Wire Line
	4200 2450 4350 2450
Wire Wire Line
	4200 2550 4350 2550
Wire Wire Line
	4200 2650 4350 2650
Wire Wire Line
	4200 2750 4350 2750
Wire Wire Line
	4200 2850 4400 2850
Wire Wire Line
	4200 2950 4400 2950
Wire Wire Line
	4200 3050 4400 3050
Wire Wire Line
	4200 3150 4400 3150
Wire Wire Line
	4200 3250 4400 3250
Wire Wire Line
	3550 2450 3700 2450
Wire Wire Line
	3550 2550 3700 2550
Wire Wire Line
	3550 2650 3700 2650
Wire Wire Line
	3550 2750 3700 2750
Wire Wire Line
	3500 2850 3700 2850
Wire Wire Line
	3500 2950 3700 2950
Wire Wire Line
	3500 3050 3700 3050
Wire Wire Line
	3500 3150 3700 3150
Wire Wire Line
	3500 3250 3700 3250
Wire Wire Line
	3500 1200 3700 1200
Wire Wire Line
	3500 1300 3700 1300
Wire Wire Line
	3500 1400 3700 1400
Wire Wire Line
	3500 1500 3700 1500
Wire Wire Line
	3500 1600 3700 1600
Wire Wire Line
	4200 1200 4400 1200
Wire Wire Line
	4200 1300 4400 1300
Wire Wire Line
	4200 1400 4400 1400
Wire Wire Line
	4200 1500 4400 1500
Wire Wire Line
	4200 1600 4400 1600
Wire Wire Line
	4200 1700 4400 1700
Wire Wire Line
	4200 1800 4400 1800
Wire Wire Line
	4200 1900 4400 1900
Wire Wire Line
	4200 2000 4400 2000
Wire Wire Line
	4200 2100 4400 2100
Wire Wire Line
	3500 1700 3700 1700
Wire Wire Line
	3500 1800 3700 1800
Wire Wire Line
	3500 1900 3700 1900
Wire Wire Line
	3500 2000 3700 2000
Wire Wire Line
	3500 2100 3700 2100
Wire Wire Line
	1750 4700 1950 4700
Wire Wire Line
	1750 4600 1950 4600
Wire Wire Line
	1950 4500 1750 4500
Wire Wire Line
	1750 4400 1950 4400
Text Label 600  5400 0    39   ~ 0
LCD1
Text Label 600  5500 0    39   ~ 0
LCD2
Text Label 600  5600 0    39   ~ 0
LCD3
Text Label 600  5700 0    39   ~ 0
LCD4
Text Label 600  5800 0    39   ~ 0
LCD5
Text Label 600  5900 0    39   ~ 0
LCD6
Text Label 600  6000 0    39   ~ 0
LCD7
Text Label 600  6100 0    39   ~ 0
LCD8
Text Label 600  6200 0    39   ~ 0
LCD9
Text Label 550  6300 0    39   ~ 0
LCD10
Text Label 550  6400 0    39   ~ 0
LCD11
Text Label 550  6500 0    39   ~ 0
LCD12
Text Label 550  6600 0    39   ~ 0
LCD13
Text Label 550  6700 0    39   ~ 0
LCD14
Text Label 550  6800 0    39   ~ 0
LCD15
Text Label 550  6900 0    39   ~ 0
LCD16
Text Label 1750 6800 0    39   ~ 0
LCD35
Text Label 550  7200 0    39   ~ 0
LCD19
Text Label 1750 5300 0    39   ~ 0
LCD20
Text Label 1750 5400 0    39   ~ 0
LCD21
Text Label 1750 5500 0    39   ~ 0
LCD22
Text Label 1750 5600 0    39   ~ 0
LCD23
Text Label 1750 5700 0    39   ~ 0
LCD24
Text Label 1750 5800 0    39   ~ 0
LCD25
Text Label 1750 5900 0    39   ~ 0
LCD26
Text Label 1750 6000 0    39   ~ 0
LCD27
Text Label 1750 6100 0    39   ~ 0
LCD28
Text Label 1750 6200 0    39   ~ 0
LCD29
Text Label 1750 6300 0    39   ~ 0
LCD30
Text Label 1750 6400 0    39   ~ 0
LCD31
Text Label 1750 6500 0    39   ~ 0
LCD32
Text Label 1750 6600 0    39   ~ 0
LCD33
Text Label 1750 6700 0    39   ~ 0
LCD34
Text Label 600  5300 0    39   ~ 0
LCD0
Text Label 550  7000 0    39   ~ 0
LCD17
Text Label 550  7100 0    39   ~ 0
LCD18
Text Label 1750 7200 0    39   ~ 0
LCD39
Text Label 1750 6900 0    39   ~ 0
LCD36
Text Label 1750 7000 0    39   ~ 0
LCD37
Text Label 1750 7100 0    39   ~ 0
LCD38
$Comp
L LCD-TOKEN LCD2
U 1 1 559B8162
P 1250 6250
F 0 "LCD2" V 100 5975 40  0000 C CNN
F 1 "LCD-TOKEN" V 2400 6075 40  0000 C CNN
F 2 "LCD-TOKEN" V 2400 6075 40  0000 C CNN
F 3 "~" H 1375 6603 60  0000 C CNN
	1    1250 6250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	600  5400 750  5400
Wire Wire Line
	600  5500 750  5500
Wire Wire Line
	750  5600 600  5600
Wire Wire Line
	600  5700 750  5700
Wire Wire Line
	750  5800 600  5800
Wire Wire Line
	600  5900 750  5900
Wire Wire Line
	750  6000 600  6000
Wire Wire Line
	600  6100 750  6100
Wire Wire Line
	750  6200 600  6200
Wire Wire Line
	550  6300 750  6300
Wire Wire Line
	550  6400 750  6400
Wire Wire Line
	550  6500 750  6500
Wire Wire Line
	550  6600 750  6600
Wire Wire Line
	550  6700 750  6700
Wire Wire Line
	550  6800 750  6800
Wire Wire Line
	550  6900 750  6900
Wire Wire Line
	1750 6800 1950 6800
Wire Wire Line
	1750 6700 1950 6700
Wire Wire Line
	1950 6600 1750 6600
Wire Wire Line
	1750 6500 1950 6500
Wire Wire Line
	1950 6400 1750 6400
Wire Wire Line
	1750 6300 1950 6300
Wire Wire Line
	1950 6200 1750 6200
Wire Wire Line
	1750 6100 1950 6100
Wire Wire Line
	1950 6000 1750 6000
Wire Wire Line
	1750 5900 1950 5900
Wire Wire Line
	1950 5800 1750 5800
Wire Wire Line
	1750 5700 1950 5700
Wire Wire Line
	1950 5600 1750 5600
Wire Wire Line
	1750 5500 1950 5500
Wire Wire Line
	1750 5400 1950 5400
Wire Wire Line
	1950 5300 1750 5300
Wire Wire Line
	550  7200 750  7200
Wire Wire Line
	550  7000 750  7000
Wire Wire Line
	600  5300 750  5300
Wire Wire Line
	550  7100 750  7100
Wire Wire Line
	1750 7200 1950 7200
Wire Wire Line
	1750 7100 1950 7100
Wire Wire Line
	1950 7000 1750 7000
Wire Wire Line
	1750 6900 1950 6900
$EndSCHEMATC
