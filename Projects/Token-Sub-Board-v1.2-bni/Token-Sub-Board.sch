EESchema Schematic File Version 2
LIBS:power
LIBS:Components
LIBS:Devices
LIBS:Connectors
LIBS:Token-Sub-Board-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "1 sep 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 4700 4050 0    40   ~ 0
C1
Text Label 4600 4050 0    40   ~ 0
C2
Text Label 4700 3950 0    40   ~ 0
C4
Text Label 4600 3950 0    40   ~ 0
C5
Text Label 4700 3750 0    40   ~ 0
C7
Text Label 4600 3750 0    40   ~ 0
C8
Text Label 4700 3450 0    40   ~ 0
C0
$Comp
L HEADER-12X2 P3
U 1 1 55623562
P 6750 1950
F 0 "P3" V 6700 1950 40  0000 C CNN
F 1 "HEADER-12X2" V 6800 1950 40  0000 C CNN
F 2 "~" H 6750 1950 60  0000 C CNN
F 3 "~" H 6750 1950 60  0000 C CNN
	1    6750 1950
	-1   0    0    -1  
$EndComp
Text Label 6400 2500 0    40   ~ 0
C1
Text Label 7100 2500 2    40   ~ 0
E1
Text Label 7100 2400 2    40   ~ 0
E4
Text Label 7100 2300 2    40   ~ 0
E7
Text Label 7100 2100 2    40   ~ 0
E0
Text Label 7100 2000 2    40   ~ 0
E8
Text Label 7100 2200 2    40   ~ 0
E5
Text Label 7100 1800 2    40   ~ 0
E2
Text Label 7100 1900 2    40   ~ 0
EE
Text Label 7100 1700 2    40   ~ 0
E9
Text Label 7100 1600 2    40   ~ 0
E6
Text Label 7100 1500 2    40   ~ 0
E3
Text Label 6400 2400 0    40   ~ 0
C4
Text Label 6400 2300 0    40   ~ 0
C7
Text Label 6400 2100 0    40   ~ 0
C0
Text Label 6400 2000 0    40   ~ 0
C8
Text Label 6400 2200 0    40   ~ 0
C5
Text Label 6400 1800 0    40   ~ 0
C2
Text Label 6400 1900 0    40   ~ 0
CE
Text Label 6400 1700 0    40   ~ 0
C9
Text Label 6400 1600 0    40   ~ 0
C6
Text Label 6400 1500 0    40   ~ 0
C3
$Comp
L VCC #PWR01
U 1 1 55624295
P 7100 1400
F 0 "#PWR01" H 7100 1500 30  0001 C CNN
F 1 "VCC" H 7100 1500 30  0000 C CNN
F 2 "" H 7100 1400 60  0000 C CNN
F 3 "" H 7100 1400 60  0000 C CNN
	1    7100 1400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 55624316
P 6300 1450
F 0 "#PWR02" H 6300 1450 30  0001 C CNN
F 1 "GND" H 6300 1380 30  0001 C CNN
F 2 "" H 6300 1450 60  0000 C CNN
F 3 "" H 6300 1450 60  0000 C CNN
	1    6300 1450
	1    0    0    -1  
$EndComp
Text Label 4500 4050 0    40   ~ 0
C3
Text Label 4500 3950 0    40   ~ 0
C6
Text Label 4500 3750 0    40   ~ 0
C9
Text Label 4600 3650 0    40   ~ 0
E1
Text Label 4500 3650 0    40   ~ 0
E4
Text Label 4400 3650 0    40   ~ 0
E7
Text Label 4700 3850 0    40   ~ 0
E2
Text Label 4600 3850 0    40   ~ 0
E5
Text Label 4500 3850 0    40   ~ 0
E8
Text Label 4700 3550 0    40   ~ 0
EE
Text Label 4700 3350 0    40   ~ 0
CE
Text Label 4700 3650 0    40   ~ 0
E0
Text Label 4600 3550 0    40   ~ 0
E3
Text Label 4500 3550 0    40   ~ 0
E6
Text Label 4400 3550 0    40   ~ 0
E9
Text Label 4800 3350 0    40   ~ 0
K1
Text Label 4800 3450 0    40   ~ 0
K2
Text Label 4800 3550 0    40   ~ 0
K3
Text Label 4800 3650 0    40   ~ 0
K4
Text Label 4800 3750 0    40   ~ 0
K5
Text Label 4800 3850 0    40   ~ 0
K6
Text Label 4800 3950 0    40   ~ 0
K7
$Comp
L VCC #PWR03
U 1 1 55CF321D
P 4500 4250
F 0 "#PWR03" H 4500 4350 30  0001 C CNN
F 1 "VCC" H 4500 4350 30  0000 C CNN
F 2 "" H 4500 4250 60  0000 C CNN
F 3 "" H 4500 4250 60  0000 C CNN
	1    4500 4250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 55CF32AE
P 4700 4300
F 0 "#PWR04" H 4700 4300 30  0001 C CNN
F 1 "GND" H 4700 4230 30  0001 C CNN
F 2 "" H 4700 4300 60  0000 C CNN
F 3 "" H 4700 4300 60  0000 C CNN
	1    4700 4300
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG05
U 1 1 55905D23
P 7200 1300
F 0 "#FLG05" H 7200 1395 30  0001 C CNN
F 1 "PWR_FLAG" H 7200 1480 30  0000 C CNN
F 2 "" H 7200 1300 60  0000 C CNN
F 3 "" H 7200 1300 60  0000 C CNN
	1    7200 1300
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG06
U 1 1 55905C01
P 6300 1300
F 0 "#FLG06" H 6300 1395 30  0001 C CNN
F 1 "PWR_FLAG" H 6300 1480 30  0000 C CNN
F 2 "" H 6300 1300 60  0000 C CNN
F 3 "" H 6300 1300 60  0000 C CNN
	1    6300 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 2500 6500 2500
Wire Wire Line
	6400 2400 6500 2400
Wire Wire Line
	6400 2300 6500 2300
Wire Wire Line
	6400 2100 6500 2100
Wire Wire Line
	6500 2000 6400 2000
Wire Wire Line
	6400 2200 6500 2200
Wire Wire Line
	6500 1800 6400 1800
Wire Wire Line
	6500 1900 6400 1900
Wire Wire Line
	6500 1700 6400 1700
Wire Wire Line
	6500 1600 6400 1600
Wire Wire Line
	6500 1500 6400 1500
Wire Wire Line
	7000 1500 7100 1500
Wire Wire Line
	7000 1600 7100 1600
Wire Wire Line
	7000 1700 7100 1700
Wire Wire Line
	7000 1900 7100 1900
Wire Wire Line
	7000 1800 7100 1800
Wire Wire Line
	7000 2200 7100 2200
Wire Wire Line
	7000 2000 7100 2000
Wire Wire Line
	7000 2100 7100 2100
Wire Wire Line
	7000 2300 7100 2300
Wire Wire Line
	7000 2400 7100 2400
Wire Wire Line
	7000 2500 7100 2500
Wire Wire Line
	7000 1400 7200 1400
Wire Wire Line
	6300 1300 6300 1450
Wire Wire Line
	6300 1400 6500 1400
Wire Wire Line
	4500 4250 4650 4250
Wire Wire Line
	4850 4250 4700 4250
Wire Wire Line
	4700 4250 4700 4300
Wire Wire Line
	4650 4250 4650 4150
Wire Wire Line
	4650 4150 4850 4150
Wire Wire Line
	7200 1400 7200 1300
Connection ~ 6300 1400
Connection ~ 7100 1400
$Comp
L HEADER-10X1 P1
U 1 1 55E524FB
P 5100 3800
F 0 "P1" V 5050 3800 40  0000 C CNN
F 1 "HEADER-10X1" V 5150 3800 40  0000 C CNN
F 2 "~" H 5100 3800 60  0000 C CNN
F 3 "~" H 5100 3800 60  0000 C CNN
	1    5100 3800
	1    0    0    -1  
$EndComp
Text Label 4800 4050 0    40   ~ 0
K8
Wire Wire Line
	4700 3350 4850 3350
Wire Wire Line
	4700 3450 4850 3450
Wire Wire Line
	4400 3550 4850 3550
Wire Wire Line
	4400 3650 4850 3650
Wire Wire Line
	4500 3750 4850 3750
Wire Wire Line
	4500 3850 4850 3850
Wire Wire Line
	4500 3950 4850 3950
Wire Wire Line
	4500 4050 4850 4050
$EndSCHEMATC
